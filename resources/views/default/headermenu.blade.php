@if (isset($menu))
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        @if(\kore\Kore::getKore()->appLogo)
            <a class="navbar-brand" href="#">
                <div><img src="/{{ \kore\Kore::getKore()->appLogo }}" style="max-width: 100px;"></div>
            </a>
        @endif
        <a class="navbar-brand" href="#">
            {{ strtoupper(\kore\Kore::getKore()->appName) }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                @foreach($menu as $m)
                    @if (isset($m['submenu']))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{$m['label']}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @foreach($m['submenu'] as $sm)
                                    <a class="nav-link"
                                       href="{{$sm['link']}}">{{$sm['label']}}</a>
                                @endforeach
                            </div>
                        </li>
                    @else
                        <li class="nav-item @if(isset($m['active']) && $m['active']) active @endif">
                            <a class="nav-link"
                               href="{{$m['link']}}">{{$m['label']}}</a>
                        </li>
                    @endif
                @endforeach

                {{--<li class="nav-item active">--}}
                    {{--<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="#">Link</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                        {{--Dropdown--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
                        {{--<a class="dropdown-item" href="#">Action</a>--}}
                        {{--<a class="dropdown-item" href="#">Another action</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">Something else here</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link disabled" href="#">Disabled</a>--}}
                {{--</li>--}}
            </ul>
            @if($user)
                <form class="navbar-text my-2 my-lg-0">
                    <div>{{$user->username}}</div>
                </form>
            @endif
        </div>
    </nav>
@endif