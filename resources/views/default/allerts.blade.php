@if (isset($alerts))
    @foreach($alerts as $a)
        <div class="alert alert-{{$a->type}}
        @if($a->dismissible) alert-dismissible fade show @endif
                " role="alert">
            <?php echo $a->content; ?>
            @if($a->dismissible)
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            @endif
        </div>
    @endforeach
@endif
@if (isset($error))
    <div class="alert alert-danger
    @if($error->dismisible) alert-dismissible fade show @endif
            " role="alert">
        <?php echo $error->content; ?>
        @if($error->dismisible)
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        @endif
    </div>
@endif