@extends("default.default")

@push('scripts')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"/>
    {{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>--}}
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"/>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    {{--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            var table = $('#custom_table').DataTable({
                responsive: true,
                data: <?php echo $data_json; ?>,
            });
        });

    </script>
@endpush

@section('content')
    <div class="container">

        <div class="container" id="buttons" style="margin: 10px;">
            @if(isset($return))
                <button type="button" class="btn btn-blue float-right" onclick="window.location.href='{{ $return }}'">Volver
                </button>
            @endif

            <button type="button" class="btn btn-primary"
                    onclick="window.location.href='{{$create}}'">Create</button>
        </div>

        @if(isset($filters) && count($filters) > 0)
            {{--{% include "@CQMViewsModule/base/defaultFilters.html.twig" %}--}}
        @endif

        <table id="custom_table" class="table table-striped table-bordered" style="width:100%; margin-top: 10px;">
            <thead>
            <tr>
                @foreach($tableFields as $f)
                    <th> {{ $f }} </th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@endsection

