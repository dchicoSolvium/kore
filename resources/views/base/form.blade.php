@extends("default.default")

@push('scripts')
    <script type=“text/javascript”
            src=“https://cdnjs.cloudflare.com/ajax/libs/hideshowpassword/2.0.8/hideShowPassword.min.js”></script>

    <style>
        .focus input, .focus select {
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .focus label {
            font-weight: bold;
        }

        .password + .glyphicon {
            cursor: pointer;
            pointer-events: all;
        }

        iframe {
            width: 100%;
            height: 400px;
            border-style: none;
        }

        .custom-file-control .selected ::after {
            content: "" !important;
        }
    </style>

    <script>
        $(document).ready(function () {
            // toggle password visibility
            $('.password + .glyphicon').on('click', function () {
                $(this).toggleClass('glyphicon-eye-close').toggleClass('glyphicon-eye-open'); // toggle our classes for the eye icon

                if ($('.password').attr('type') == 'password') {
                    $('.password').attr('type', 'text');
                } else {
                    $('.password').attr('type', 'password');
                }

            });

            $('.custom-file-input').on('change', function () {
                var fileName = $(this).val();
                var label = $('label[for="' + $(this).attr('id') + '"]');
                label.html(fileName);
            })
        });
    </script>
@endpush

@section('content')

    <div class="container">
        <form id='form' method="post" action="parse" enctype="multipart/form-data">
            @foreach($fields  as $f)
                @if($f->kType->getName() == 'kId' )
                    @continue
                @endif
                <div class="form-group row
                    @if($f->focus) focus @endif

                @if($f->kType->getName() == 'kPass')
                        has-feedback
                @endif "

                     @if($f->help)
                     title="{{$f->help}}"
                        @endif
                >

                    @if($f->kType->getName() == 'kKidFrame')
                        <iframe class="embed-responsive-item" src="{{$f->link}}" width="100%" height="500px"
                                seamless=""></iframe></div>
                @continue
                @endif

                @if( $f->label == null )
                    <label $for="{{ $f->getName() }}" class="col-sm-2 col-form-label">{{ $f->getName() }}:</label>
                @else
                    <label $for="{{ $f->getName() }}" class="col-sm-2 col-form-label">{{ $f->label }}:</label>
                @endif

                @if($f->kType->getName() == 'kString' )
                    <input type="text" class="form-control col-sm-10"
                           name="{{ $f->getName() }}" value="{{ $entity->{$f->getName()} }}"
                           @if($f->disabled) disabled @endif>
                @elseif($f->kType->getName() == 'kPass' )
                    <input type="password" class="form-control col-sm-10 password"
                           name="{{ $f->getName() }}"
                           value="{{ $entity->{$f->getName()} }}" @if($f->disabled) disabled @endif>
                    <i class="glyphicon glyphicon-eye-open form-control-feedback" style="top: 0;"></i>
                @elseif($f->kType->getName() == 'kInt' )
                    <input type="number" class="form-control col-sm-10"
                           name="{{ $f->getName() }}"
                           value="{{ $entity->{$f->getName()} }}" @if($f->disabled) disabled @endif>
                @elseif($f->kType->getName() == 'kFile' )
                    <div class="input-group col-sm-10" style="padding: 0px;">
                        @if($entity->{$f->getName()})
                            <div class="input-group-prepend">
                                <a class="btn btn-outline-secondary" href="/{{ $entity->{$f->getName()} }}" download>
                                    Download
                                </a>
                            </div>
                        @endif
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="{{ $f->getName() }}"
                                   id="{{ $f->getName() }}-id" aria-describedby="inputGroupFileAddon03">
                            @if($entity->{$f->getName()})
                                <label class="custom-file-label"
                                       for="{{ $f->getName() }}-id">{{ $entity->{$f->getName()} }}</label>
                            @else
                                <label class="custom-file-label" for="{{ $f->getName() }}-id">Choose file</label>
                            @endif
                        </div>
                    </div>
                @elseif($f->kType->getName() == 'kTime' )
                    <input type="time" class="form-control col-sm-10"
                           name="{{ $f->getName() }}"
                           value="{{ \kore\Kore::kTime()->unparse($entity->{$f->getName()}) }}" @if($f->disabled) disabled @endif>
                @elseif($f->kType->getName() == 'kBool' )
                    <input type="hidden" name="{{ $f->getName() }}" value=false/>
                    <input type="checkbox" name="{{ $f->getName() }}" value=true
                           @if( $entity->{$f->getName()} )
                           checked
                            @endif />
                @elseif($f->kType->getName() == 'kDate' )
                    <?php
                    if ($entity->{$f->getName()} == null) {
                        $entity->{$f->getName()} = new DateTime();
                    }
                    ?>
                    <input type="date" class="form-control col-sm-10"
                           name="{{ $f->getName() }}"
                           value="{{ $entity->{$f->getName()}->format('Y-m-d') }}" @if($f->disabled) disabled @endif>
                @elseif($f->kType->getName() == 'kDatetime' )
                    <?php
                    if ($entity->{$f->getName()} == null) {
                        $entity->{$f->getName()} = new DateTime();
                    }
                    ?>
                    <input type="date" class="form-control col-sm-6"
                           name="{{ $f->getName() }}"
                           value="{{ $entity->{$f->getName()}->format('Y-m-d') }}" @if($f->disabled) disabled @endif>
                    <span class="col-sm-1">A las</span>
                    <input type="number" class="form-control col-sm-1"
                           name="{{ $f->getName() }}-hour" max="24" min="0"
                           value="{{ $entity->{$f->getName()}->format('H') }}" @if($f->disabled) disabled @endif>
                    <span class="col-sm-1">:</span>
                    <input type="number" class="form-control col-sm-1"
                           name="{{ $f->getName() }}-minute" max="60" min="0"
                           value="{{ $entity->{$f->getName()}->format('i') }}" @if($f->disabled) disabled @endif>
                @elseif($f->kType->getName() == 'kSelect' || $f->kType->getName() == 'kFK' )
                    <select name="{{$f->getName()}}" class="form-control col-sm-10" @if($f->disabled) disabled @endif>
                        <option value="">...</option>
                        @foreach($f->options as $v => $o)
                            <option value="{{ $v }}"
                                    @if($entity->{$f->getName()} == $v) selected @endif>{{ $o }}</option>
                        @endforeach
                    </select>
                @elseif($f->kType->getName() == 'kColor' )
                    <input type="color" class="form-control col-sm-10"
                           name="{{ $f->getName() }}" value="{{ $entity->{$f->getName()} }}"
                           @if($f->disabled) disabled @endif>
        @endif
    </div>
    @endforeach
    </div>

    <input type="hidden" value={{ $id }} name=id>

    <input type="hidden" value=true name="formSend">

    @if (isset($nomenu))
        <input type="hidden" value=true name="nomenu">
    @endif

    <button type="submit" class="btn btn-primary float-right">Guardar</button>
    <button type="reset"
            onclick="location.href='{{\kore\Kore::htmlRenderer()->createURL('table', -1, $model->getName())}}'"
            class="btn btn-secondary  float-right" style="margin-right: 2px;">Volver
    </button>
    </form>
    </div>
@endsection

