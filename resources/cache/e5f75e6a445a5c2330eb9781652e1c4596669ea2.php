<?php $__env->startPush('scripts'); ?>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"/>
    
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"/>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

    <?php
    $conceptos = \kore\Kore::getDriver()->list(\kore\Kore::concepto());

    $json_concepts = $data_json;


    ?>

    <script type="text/javascript">

        var concepts = <?php echo $json_concepts; ?>;

        function getConceptById(id){
            for (i = 0; i < concepts.length; i++){
                if (concepts[i].id == id){
                    return concepts[i];
                }
            }
        }

        $(document).ready(function () {
            var table = $('#custom_table').DataTable({
                responsive: true,
                "scrollX": true,
                data: <?php echo $data_json; ?>,
                "createdRow": function( row, data, dataIndex){
                    concept = getConceptById(data[6]);

                    if (concept == null){
                        $(row).addClass('dt_employe_no_concept');
                        return;
                    }

                    $(row).css('background-color', concept.color);

                    if (concept.tipoConcepto == '1'){
                        $(row).addClass('dt_employe_trabajo');
                    }else if (concept.tipoConcepto == '2'){
                        $(row).addClass('dt_employe_fin_jornada');
                    }else if (concept.tipoConcepto == '3'){
                        $(row).addClass('dt_employe_pausa');
                    }

                    if (concept.catConcepto == '1'){
                        $(row).addClass('dt_employe_normal');
                    }else if (concept.catConcepto == '2'){
                        $(row).addClass('dt_employe_ausencia');
                    }else if (concept.catConcepto == '3'){
                        $(row).addClass('dt_employe_absentismo');
                    }

                }
            });
        });

    </script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">

        <div class="container" id="buttons" style="margin: 10px;">
            <?php if(isset($return)): ?>
                <button type="button" class="btn btn-blue float-right" onclick="window.location.href='<?php echo e($return); ?>'">Volver
                </button>
            <?php endif; ?>

            <button type="button" class="btn btn-primary"
                    onclick="window.location.href='<?php echo e($create); ?>'">Insertar</button>
        </div>

        <?php if(isset($filters) && count($filters) > 0): ?>
            
        <?php endif; ?>

        <table id="custom_table" class="table table-striped table-bordered" style="width:100%; margin-top: 10px;">
            <thead>
            <tr>
                <?php $__currentLoopData = $tableFields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $f): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <th> <?php echo e($f); ?> </th>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make("default.default", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/empleados/table.blade.php ENDPATH**/ ?>