<?php $__env->startPush('scripts'); ?>

    <link rel="stylesheet" href="/bower_components/bootstrap/less/bootstrap.less">
    <link rel="stylesheet" href="/bower_components/bootstrap-calendar/css/calendar.css">

    <script src="https://kit.fontawesome.com/3cd1263fea.js"></script>
    <link rel="stylesheet" href="https://kit-free.fontawesome.com/releases/latest/css/free.min.css" media="all">

    <script type="text/javascript" src="/bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="/bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script type="text/javascript" src="/bower_components/underscore/underscore.js"></script>
    <script type="text/javascript" src="/bower_components/moment/moment.js"></script>
    <script type="text/javascript" src="/bower_components/bootstrap-calendar/js/calendar.js"></script>

    <script type="text/javascript" src="/bower_components/bootstrap-calendar/js/language/es-ES.js"></script>

    <script type="text/javascript">

        var fichajes = <?php echo json_encode(array_reverse($data)); ?>;
        <?php
                // Parseamos los conceptos
                $conceptsE = \kore\Kore::getDriver()->list(\kore\Kore::concepto());
                $concepts = [];
                foreach ($conceptsE as $c){
                    $aux = [];
                    $aux['desc'] = $c->desc;
                    $aux['color'] = $c->color;
                    $aux['next'] = $c->next;
                    $concepts[] = $aux;
                }
            ?>
        var conceptos = <?php echo json_encode($concepts); ?>;
        var fichajesToday = [];
        var currentDate = new Date();
        var topHour = 7;
        var lastHour = 27;
        var lastTop = 0;


        var minTop = 35;

        var calendar = null;


        function getConcept(idConcept) {
            for (i = 0; i < conceptos.length; i++){
                if (conceptos[i].desc == idConcept){
                    return conceptos[i];
                }
            }
        }

        function dateToString(d) {
            let month = String(d.getMonth() + 1);
            let day = String(d.getDate());
            const year = String(d.getFullYear());

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return year + '-' + month + '-' + day;
        }

        function dateToStringSpain(d) {
            let month = String(d.getMonth() + 1);
            let day = String(d.getDate());
            const year = String(d.getFullYear());

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return day+ '-' + month + '-' + year;
        }

        function setDate(date) {
            currentDate = date;

            // Modificamos el campo hidden "horaFichaje" para que tenga la fecha seleccionada
            $('#horaFichaje').val(dateToString(currentDate));

            $('#title').html('Fichajes del día ' + dateToStringSpain(currentDate));

            setFichajesToday();
        }

        function setFichajesToday(){
            fichajesToday = [];

            lastTop = 0;

            fichajes.forEach( function(val, i, array) {
                date = new Date(val[0]);
                today = currentDate;

                if (date.getDate() == today.getDate() && date.getMonth() == today.getMonth()){
                    fichajesToday.push(val);
                }
            });

            var actList = $('#activity-list');

            actList.html('');

            var concept = null;

            fichajesToday.forEach(function(val, i, array) {
                date = new Date(val[0]);

                var h = date.getHours();
                var m = date.getMinutes();

                var html = '<div class="activity" id="act-' + i + '">\n' +
                    '                <div class="hour float-left">' + h + ':' + m + '</div>\n' +
                    '                <div class="activity-box">\n' +
                    '                    <div class="description float-right">' + val[2] + '</div>\n' +
                    '                </div>\n' +
                    '            </div>';

                actList.append(html);

                var act = $('#act-' + i);

                var top = (h * 60 + m - topHour * 60) * 960 / ((lastHour - topHour) * 60);

                top = Math.max(top, lastTop + minTop);

                act.css('top', top + 'px' );

                var actBox = $('#act-' + i + ' .activity-box').css('height', '0px');

                if (i - 1 >= 0){
                    var height = top - lastTop - 5;

                    var lactBox = $('#act-' + (i-1) + ' .activity-box');
                    lactBox.css('height', height + 'px');

                }

                lastTop = top;

                concept = getConcept(val[2]);

                if (concept.color){
                    actBox.css('background-color', concept.color);
                }else{
                    actBox.css('background-color', 'rgba(235, 235, 19, 0.4)');
                }

            });

            if (concept != null && concept.next){
                $('#idConcepto').val(concept.next);
            }else{
                $('#idConcepto option:first-child').attr("selected", "selected");
            }


            $('.btn-fichar').css('top', lastTop + 50 + 'px');

            $('.timeline').css('height', lastTop + 150 + 'px');
            $('.timeline .line').css('height', lastTop + 147 + 'px');
        }

        function setCalendarFuncts(){
            $('.cal-cell').click(function() {
                var view = $('[data-cal-date]', this).data('cal-view');
                var day = $('[data-cal-date]', this).data('cal-date');

                setDate(new Date(day));

                $('.cal-day-today').removeClass('cal-day-today');
                $(this).addClass('cal-day-today');
            });
        }

        $(document).ready(function () {
            calendar = $("#calendar").calendar(
                {
                    tmpl_path: "/bower_components/bootstrap-calendar/tmpls/",
                    events_source: function () { return []; },
                    language: 'es-ES'
                });

            $('.btn-fichar').click(function () {
                date = new Date();

                var h = date.getHours();
                var m = date.getMinutes();

                $('#horaFichaje-hour').val(h);
                $('#horaFichaje-minute').val(m);
            });

            setCalendarFuncts();

            setDate(new Date());

        });
    </script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    
        
            
                
            
        
        
            
                
                    
                
                
                    
                
            
        
    


    <div class="card card-container mx-auto">
        <a data-toggle="collapse" href="#collapse-calendar" role="button" aria-expanded="false" aria-controls="collapseExample">
            <div class="card-header" id="headingOne">
                <span class="mx-auto">Calendario</span>
            </div>
        </a>
        <div class="collapse" id="collapse-calendar">
            <div class="card card-body">
                <div id="calendar"></div>
            </div>
        </div>
    </div>


    <div class="timeline mx-auto">
        <div class="background">
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
            <div class="strype strype-dark"></div>
            <div class="strype strype-white"></div>
        </div>

        <div class="line"></div>
        <div id="activity-list">
            <div class="activity" style="top: 25px">
                <div class="hour float-left">08:00</div>
                <div class="activity-box" style="
                        margin-top: 0px;
                        height: 170px;
                        background-color: rgba(88, 202, 145, 0.4);
                    ">
                    <div class="description float-right">Inicio de jornada</div>
                </div>
            </div>
            <div class="activity" style="top: 205px">
                <div class="hour float-left">14:00</div>
                <div class="activity-box" style="
                        height: 98px;
                        background-color: rgba(235, 235, 19, 0.4);
                    ">
                    <div class="description float-right">Pausa a comer</div>
                </div>
            </div>
            <div class="activity" style="top: 313px">
                <div class="hour float-left">15:23</div>
                <div class="activity-box" style="
                        height: 229px;
                        background-color: rgba(88, 202, 145, 0.4);
                    ">
                    <div class="description float-right">Trabajo</div>
                </div>
            </div>
            <div class="activity" style="top: 552px">
                <div class="hour float-left">17:56</div>
                <div class="description float-right">Fin de jornada</div>
            </div>
        </div>

        
            
        
    </div>


    <!-- Modal -->
    <div class="modal fade" id="mdl-fichaje" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo fichaje</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php echo $__env->make("fichaje.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>


<?php echo $__env->make("default.default", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/fichaje/historia.blade.php ENDPATH**/ ?>