<?php $__env->startPush('scripts'); ?>
    <?php
    // Parseamos los conceptos
    $conceptsE = \kore\Kore::getDriver()->list(\kore\Kore::concepto());
    $concepts = [];
    foreach ($conceptsE as $c){
        $aux = [];
        $aux['id'] = $c->id;
        $aux['desc'] = $c->desc;
        $aux['color'] = $c->color;
        $aux['next'] = $c->next;
        $aux['tipoConcepto'] = intval($c->tipoConcepto);
        $concepts[] = $aux;
    }
    ?>
    <script type="text/javascript" src="/bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="/bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script type="text/javascript" src="/bower_components/underscore/underscore.js"></script>
    <script type="text/javascript" src="/bower_components/moment/moment.js"></script>

    <script type="text/javascript">

        var fichajes = <?php echo json_encode(array_reverse($data)); ?>;

        var conceptos = <?php echo json_encode($concepts); ?>;
        var fichajesToday = [];
        var currentDate = new Date();
        var maxTime = 10*60;
        var lastActivity = null;
        var topHour = 7;
        var lastHour = 27;
        var lastTop = 0;
        var workedTime = 0;
        var setTimerFlag = false;


        var minTop = 35;

        var calendar = null;


        function getConceptByDesc(descConcept) {
            for (i = 0; i < conceptos.length; i++){
                if (conceptos[i].desc == descConcept){
                    return conceptos[i];
                }
            }
        }

        function getConceptById(idConcept) {
            for (i = 0; i < conceptos.length; i++){
                if (conceptos[i].id == idConcept){
                    return conceptos[i];
                }
            }
        }

        function dateToString(d) {
            let month = String(d.getMonth() + 1);
            let day = String(d.getDate());
            const year = String(d.getFullYear());

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return year + '-' + month + '-' + day;
        }

        function dateToStringSpain(d) {
            let month = String(d.getMonth() + 1);
            let day = String(d.getDate());
            const year = String(d.getFullYear());

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return day+ '-' + month + '-' + year;
        }

        function setDate(date) {
            currentDate = date;

            // Modificamos el campo hidden "horaFichaje" para que tenga la fecha seleccionada
            $('#horaFichaje').val(dateToString(currentDate));

            $('#title').html('Fichajes del día ' + dateToStringSpain(currentDate));

            setFichajesToday();
        }

        function setWorkedTime() {
            workedTime = 0;
            fichajesToday.forEach( function(val, i, array) {
                var cnp = getConceptByDesc(val[2]);
                if (cnp.tipoConcepto == 1){
                    if (i + 1 < fichajesToday.length){
                        dateAux = new Date(array[i+1][0]);
                        var nextTime = dateAux.getHours() * 60 + dateAux.getMinutes();
                    }else{
                        dateAux = new Date();
                        var nextTime = dateAux.getHours() * 60 + dateAux.getMinutes();
                    }

                    dateAux = new Date(val[0]);
                    var actTime = dateAux.getHours() * 60 + dateAux.getMinutes();

                    workedTime += nextTime - actTime;
                }
            });

            var percent = workedTime / maxTime * 100;
            $('.progress-bar').css('width', percent + '%');

            var h = Math.floor(workedTime / 60);
            var m = workedTime % 60;

            var string = '';

            if (h <= 9){
                string = string + '0' + h;
            }else{
                string = string + h;
            }

            if (m <= 9){
                string = string + ':0' + m;
            }else{
                string = string + ':' + m;
            }

            $('.progress-bar').html(string);
        }

        function updateTimer() {
            if (setTimerFlag){
                $('.current-time').html('');
                return;
            }

            var ldate = new Date(lastActivity[0]);
            var cdate = new Date();

            var lmins = ldate.getHours()*60 + ldate.getMinutes();
            var cdate = cdate.getHours()*60 + cdate.getMinutes();

            var diff = cdate - lmins;

            var h = Math.floor(diff / 60);
            var m = diff % 60;

            var string = '';

            if (h <= 9){
                string = string + '0' + h;
            }else{
                string = string + h;
            }

            if (m <= 9){
                string = string + ':0' + m;
            }else{
                string = string + ':' + m;
            }

            $('.current-time').html(string);

            setWorkedTime();
        }

        function setCurrentState() {
            if (fichajesToday.length == 0){
                $('.state-line').css('background-color', 'Grey');
                $('.current-activity').html('Aún no has iniciado actividad');

                var nextConcept = conceptos[0];

                $('.button-fichar-next').css('background-color', nextConcept.color);
                $('.button-fichar-next').html(nextConcept.desc);

                $('#idConcepto').val(nextConcept.id);
                setTimerFlag = true;
            }else{
                lastActivity = fichajesToday[fichajesToday.length - 1];

                var concept = getConceptByDesc(lastActivity[2]);
                var nextConcept = getConceptById(concept.next);

                $('.state-line').css('background-color', concept.color);
                $('.current-activity').html(concept.desc);

                // var c = nextConcept.color.substring(1);      // strip #
                // var rgb = parseInt(c, 16);   // convert rrggbb to decimal
                // var r = (rgb >> 16) & 0xff;  // extract red
                // var g = (rgb >>  8) & 0xff;  // extract green
                // var b = (rgb >>  0) & 0xff;  // extract blue
                //
                // var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
                //
                // console.log(luma);
                // if (luma < 40) {
                //     $('.button-fichar-next').css('color', '#fff');
                // }else{
                //     $('.button-fichar-next').css('color', '#000');
                // }

                if (concept.tipoConcepto == 2){
                    $('.button-fichar').hide();
                    setTimerFlag = true;
                }else{
                    $('#idConcepto').val(concept.next);

                    $('.button-fichar-next').css('background-color', nextConcept.color);
                    $('.button-fichar-next').html(nextConcept.desc);
                }

            }

        }


        function setFichajesToday(){
            fichajesToday = [];
            lastTop = 0;

            fichajes.forEach( function(val, i, array) {
                date = new Date(val[0]);
                today = currentDate;

                if (date.getDate() == today.getDate() && date.getMonth() == today.getMonth()){
                    var cnp = getConceptByDesc(val[2]);
                    fichajesToday.push(val);
                }
            });
        }


        $(document).ready(function () {

            $('.button-fichar-next').click(function () {
                date = new Date();

                var h = date.getHours();
                var m = date.getMinutes();

                $('#horaFichaje-hour').val(h);
                $('#horaFichaje-minute').val(m);

                $('#form').submit();
            });

            $('.button-fichar-open').click(function () {
                date = new Date();

                var h = date.getHours();
                var m = date.getMinutes();

                $('#horaFichaje-hour').val(h);
                $('#horaFichaje-minute').val(m);
            });

            $('#title').html('');

            setFichajesToday();
            setWorkedTime();
            setCurrentState();
            updateTimer();
            setInterval(updateTimer, 10000);

        });
    </script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>


    <div class="state-line">
        <div class="current-activity">Trabajando</div>
        <div class="current-time">00:16</div>
    </div>

    <div class="row">
        <div class="button-fichar col-sm-7 center-block">
            <div class="button-fichar-next col-7">Descansar</div>
            <div class="button-fichar-open col-4" data-toggle="modal" data-target="#mdl-fichaje">Fichar</div>
        </div>
    </div>


    <p>Tiempo trabajado</p>
    <div class="progress">
        <div class="progress-bar" style="width:70%">70%</div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="mdl-fichaje" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo fichaje</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php echo $__env->make("fichaje.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>


<?php echo $__env->make("default.default", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/fichaje/fichajes.blade.php ENDPATH**/ ?>