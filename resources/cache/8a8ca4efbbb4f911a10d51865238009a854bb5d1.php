<div class="container">
    <form id="form" method="post" action="reportes" enctype="multipart/form-data">

        <div class="row">
            <label $for="iniDate" class="col-form-label col-sm-1">De</label>
            <input type="date" class="form-control col-sm-4"
                   name="iniDate" value=<?php echo e($iniDate->format('Y-m-d')); ?>>
            <label $for="endDate" class="col-form-label col-sm-1">A</label>
            <input type="date" class="form-control col-sm-4"
                   name="endDate" value=<?php echo e($endDate->format('Y-m-d')); ?>>
            <div class="align-baseline col-sm-2">
                <button type="submit" class="btn btn-primary float-left align-baseline">Calcular</button>
            </div>
        </div>

        <div class="row" style="margin-top: 50px; margin-bottom: 20px;">
            <?php if(count($reportes['empresas']) > 1): ?>
                <div class="form-group col-sm-4">
                    <label $for="empresa" class="col-form-label">Empresa: </label>
                    <select name="empresa" id="empresa-selector" class="form-control">
                        <option value="-1">...</option>
                        <?php $__currentLoopData = $reportes['empresas']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $e): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($id); ?>"><?php echo e($e['datos']['nombre']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group col-sm-4">
                    <label $for="centro" class="col-form-label">Centro de trabajo:</label>
                    <select name="centro" id="centro-selector" class="form-control">
                        <option value="-1">...</option>
                    </select>
                </div>
            <?php elseif(count($reportes['empresas']) == 1): ?>
                <input type="hidden" name="empresa" id="empresa-selector" value=0>
                <div class="form-group col-sm-4">
                    <label $for="centro" class="col-form-label">Centro de trabajo:</label>
                    <select name="centro" id="centro-selector" class="form-control">
                        <option value="-1">...</option>
                        <?php $__currentLoopData = $reportes['empresas'][0]['centros']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($id); ?>"><?php echo e($c['datos']['nombre']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            <?php endif; ?>

            <div class="form-group col-sm-4">
                <label $for="day" class="col-form-label">Día</label>
                <input type="date" class="form-control"
                       name="day" id="seleccted-day" value=<?php echo e($endDate->format('Y-m-d')); ?>

                        min="<?php echo e($iniDate->format('Y-m-d')); ?>" max="<?php echo e($endDate->format('Y-m-d')); ?>">
            </div>
        </div>

    </form>
</div>
<?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/reportes/seccion/form.blade.php ENDPATH**/ ?>