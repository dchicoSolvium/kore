<?php if(isset($menu)): ?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <?php if(\kore\Kore::getKore()->appLogo): ?>
            <a class="navbar-brand" href="#">
                <div><img src="/<?php echo e(\kore\Kore::getKore()->appLogo); ?>" style="max-width: 100px;"></div>
            </a>
        <?php endif; ?>
        <a class="navbar-brand" href="#">
            <?php echo e(strtoupper(\kore\Kore::getKore()->appName)); ?>

        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(isset($m['submenu'])): ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo e($m['label']); ?>

                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
                                <?php $__currentLoopData = $m['submenu']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a class="nav-link"
                                       href="<?php echo e($sm['link']); ?>"><?php echo e($sm['label']); ?></a>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </li>
                    <?php else: ?>
                        <li class="nav-item <?php if(isset($m['active']) && $m['active']): ?> active <?php endif; ?>">
                            <a class="nav-link"
                               href="<?php echo e($m['link']); ?>"><?php echo e($m['label']); ?></a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
            <?php if($user): ?>
                <form class="user-info navbar-text my-2 my-lg-0">
                    <?php if(count($notice) > 0): ?>
                        <a href="#" class="notice-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="notice">
                                <span>
                                    <?php echo e($user->username); ?>

                                    <span class="badge badge-red"><?php echo e(count($notice)); ?></span>
                                </span>

                                <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
                                    <?php $__currentLoopData = $notice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $n): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="nav-link">
                                            <div class="message"><?php echo e($n->message); ?></div>
                                            <div class="actions">
                                                <a href="/notification/<?php echo e($n->id); ?>/noticeAccept?reAction=<?php echo e($action); ?>&reModel=<?php echo e($model->getName()); ?>">
                                                    <i title="Confirmar" class="far fa-check-square"></i>
                                                </a>
                                                <?php if($n->canBePosposed): ?>
                                                    <a href="/notification/<?php echo e($n->id); ?>/noticePospone?reAction=<?php echo e($action); ?>&reModel=<?php echo e($model->getName()); ?>">
                                                        <i title="Posponer" class="fas fa-archive"></i>
                                                    </a>
                                                <?php endif; ?>
                                                <?php if($n->canBeDissmised): ?>
                                                    <a href="/notification/<?php echo e($n->id); ?>/noticeDissmiss?reAction=<?php echo e($action); ?>&reModel=<?php echo e($model->getName()); ?>">
                                                        <i title="Rechazar" class="fas fa-window-close"></i>
                                                    </a>
                                                <?php endif; ?>
                                                <i title="Ver" class="far fa-eye"></i>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </a>
                    <?php else: ?>
                        <div><?php echo e($user->username); ?></div>
                    <?php endif; ?>

                    <a class="change-pass" href="#" title="Cambiar contraseña" data-toggle="modal" data-target="#mdl-password" style="cursor: pointer;">Cambiar contraseña</a>
                </form>

            <?php endif; ?>
        </div>
    </nav>

    <div class="modal fade" id="mdl-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cambiar contraseña</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="<?php echo e(\kore\Kore::htmlRenderer()->createURL('changePassword')); ?>" method="post">
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="password" class="form-control col-sm-10 mx-auto" name="oldPassword" placeholder="Contraseña actual" required>
                        </div>
                        <div class="form-group row">
                            <input type="password" class="form-control col-sm-10  mx-auto" name="newPassword" placeholder="Nueva contraseña" required>
                        </div>

                        <input type="hidden" name="reAction" value="<?php echo e($action); ?>">
                        <input type="hidden" name="reModel" value="<?php echo e($model->getName()); ?>">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary float-right">Guardar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
<?php endif; ?><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/default/headermenu.blade.php ENDPATH**/ ?>