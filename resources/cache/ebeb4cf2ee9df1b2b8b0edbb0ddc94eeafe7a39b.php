<?php $__env->startPush('scripts'); ?>
    <script type=“text/javascript”
            src=“https://cdnjs.cloudflare.com/ajax/libs/hideshowpassword/2.0.8/hideShowPassword.min.js”></script>

    <style>
        .focus input, .focus select {
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .focus label {
            font-weight: bold;
        }

        .custom-file-control .selected ::after {
            content: "" !important;
        }
    </style>

    <script>
        $(document).ready(function () {
            $('.custom-file-input').on('change',function(){
                var fileName = $(this).val();
                var label = $('label[for="' + $(this).attr('id') + '"]');
                label.html(fileName);
            })
        });
    </script>
<?php $__env->stopPush(); ?>

<form id="form" method="post" action="parse" enctype="multipart/form-data">
    <div class="modal-body">
            <div class="form-group row">
                <label $for="idConcepto" class="col-sm-3 col-form-label">Concepto:</label>

                <select name="idConcepto" id="idConcepto" class="form-control col-sm-7">
                    <?php $__currentLoopData = \kore\Kore::getKModel()->idConcepto->options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v => $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($v); ?>" ><?php echo e($o); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>

            </div>

            
                
                    
                        
                            
                        
                    
                

                
                    
                        
                        
                        
                        
                        
                    

                    
                        
                        
                            
                                
                                
                            
                        
                    
                
            

            <input type="hidden" id="horaFichaje" name="horaFichaje" value="<?php echo e(date("Y-m-j")); ?>">
            <input type="hidden" class="form-control col-sm-3" id="horaFichaje-hour" name="horaFichaje-hour" max="24" min="0" value="12">
            <input type="hidden" class="form-control col-sm-3" id="horaFichaje-minute" name="horaFichaje-minute" max="60" min="0" value="55">

            <input type="hidden" value="<?php echo e($user->id); ?>" name="idEmpleado">

            <input type="hidden" value="-1" name="id">

            <input type="hidden" value="true" name="formSend">


            
            
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary float-right">Fichar</button>
    </div>
</form><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/fichaje/form.blade.php ENDPATH**/ ?>