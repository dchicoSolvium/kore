<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">

        var reportes = <?php echo json_encode($reportes); ?>;
        var conceptos = <?php echo json_encode($conceptos); ?>;

        var iniHour = 7;
        var endHour = 23;

        var byConceptGraph = null;
        var byCatConceptGraph = null;

        var empleados = [];

        $(document).ready(function () {
            $('#empresa-selector').change(function () {
                var empresa = $('#empresa-selector').val();
                var centros = reportes.empresas[empresa].centros;

                $('#centro-selector').empty();
                $('#centro-selector').append('<option value="-1">...</option>');

                for (i = 0; i < centros.length; i++) {
                    $('#centro-selector').append('<option value="' + i + '" >' + centros[i].datos.nombre + '</option>');
                }
            });


            $('.detailTable .employeeData .hours').empty();
            for (i = iniHour; i < endHour; i = i + 2) {
                if (i < 10) {
                    div = '<div class="hour col">0' + i + ':00</div>';
                } else {
                    div = '<div class="hour col">' + i + ':00</div>';
                }

                $('.detailTable .employeeData .hours').append(div);
            }

            $('.employeeDataContent').css('width', (endHour - iniHour) * 100 / 2 + 'px');

            $('#seleccted-day').change(function () {
                populateGraph();
            });

            $('#empresa-selector').change(function () {
                populateGraph();
            });

            $('#centro-selector').change(function () {
                populateGraph();
            });

            populateGraph();
        });


        function getDia(empleado) {
            var dia = $('#seleccted-day').val();

            for (i = 0; i < empleado.dias.length; i++) {
                if (empleado.dias[i].dia == dia) {
                    return empleado.dias[i];
                }
            }
        }

        function populateEmployee(empleado) {
            var dia = getDia(empleado);

            $('#detail-names-block').append('<div class="detail-row">' +
                empleado.datos.nombre + ' ' + empleado.datos.apellidos +
                '</div>');

            var lastRow = $(".detail-row").last()

            if ($(".detail-row").last().hasClass("strype-dark")) {
                var div = '<div class="detail-row strype-white">';
            } else {
                var div = '<div class="detail-row strype-dark">';
            }

            var last = null;
            var lTime = -1;

            for (i = 0; i < dia.fichajes.length; i++) {
                var f = dia.fichajes[i];

                if (last == null) {
                    var lTime = iniHour * 60;
                    var color = 'rgba(0,0,0,0)'
                } else {
                    var color = last.concepto.color;
                }

                var date = new Date(f.horaFichaje.date);
                var cTime = date.getHours() * 60 + date.getMinutes();

                div += '<div class="fichaje-detail" style="' +
                    'width: ' + (((cTime - lTime) / 60) / 2) * 100 + 'px' +
                    '; background-color: ' + color + ';"></div>';

                last = f;
                lTime = cTime;
            }

            $('#detail-data-block').append(div + '</div>');
        }

        function populateGraph() {
            var empresa = $('#empresa-selector').val();
            var centro = $('#centro-selector').val();

            empleados = [];
            var rep = null;

            if (centro != -1 && empresa != -1) {
                empleados = reportes.empresas[empresa].centros[centro].empleados;
                rep = reportes.empresas[empresa].centros[centro];
            } else if (empresa != -1) {
                centros = reportes.empresas[empresa].centros;

                for (i = 0; i < centros.length; i++) {
                    empleados = empleados.concat(centros[i].empleados);
                }

                rep = reportes.empresas[empresa];
            } else {
                empresas = reportes.empresas;

                for (i = 0; i < empresas.length; i++) {
                    centros = reportes.empresas[i].centros;

                    for (j = 0; j < centros.length; j++) {
                        empleados = empleados.concat(centros[j].empleados);
                    }
                }
            }

            $('#detail-names-block').empty();
            $('#detail-data-block').empty();


            for (var i = 0; i < empleados.length; i++) {
                console.log(empleados[i].datos.nombre);
                populateEmployee(empleados[i]);
                console.log(i);
            }

            if (rep != null) {
                var data = [];
                var labels = [];
                var colors = [];

                for (var c in rep.estadisticas.horasConcepto) {
                    labels.push(conceptos[c].desc);
                    data.push(rep.estadisticas.horasConcepto[c]) / 60;
                    colors.push(conceptos[c].color);
                }

                if (byConceptGraph) {
                    byConceptGraph.destroy();
                }

                if (labels.length > 0) {
                    byConceptGraph = new Chart($('#hoursByConcept'), {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                data: data,
                                backgroundColor: colors,
                                borderColor: colors,
                            }],

                            // These labels appear in the legend and in the tooltips when hovering different arcs
                            labels: labels
                        },
                        options: {}
                    });
                }


                data = [];
                labels = [];
                colors = [];
                var typeConceptos = {1: 'Trabajo', 2: 'Fin de jornada', 3: 'Pausa'};
                var colsConceptos = {1: 'rgba(88, 202, 145, 0.4)', 2: 'rgba(235, 235, 19, 0.4)', 3: 'aliceblue'};

                for (var c in rep.estadisticas.horasTipoConcepto) {
                    labels.push(typeConceptos[c]);
                    data.push(rep.estadisticas.horasTipoConcepto[c]) / 60;
                    colors.push(colsConceptos[c]);
                }

                if (byCatConceptGraph) {
                    byCatConceptGraph.destroy();
                }

                if (labels.length > 0) {
                    byCatConceptGraph = new Chart($('#hoursByCatConcept'), {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                data: data,
                                backgroundColor: colors,
                                borderColor: colors,
                            }],

                            // These labels appear in the legend and in the tooltips when hovering different arcs
                            labels: labels
                        },
                        options: {}
                    });
                }
            }


        }
    </script>
<?php $__env->stopPush(); ?><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/reportes/seccion/javascript.blade.php ENDPATH**/ ?>