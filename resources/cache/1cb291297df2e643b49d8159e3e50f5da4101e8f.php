<?php if(isset($alerts)): ?>
    <?php $__currentLoopData = $alerts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="alert alert-<?php echo e($a->type); ?>

        <?php if($a->dismissible): ?> alert-dismissible fade show <?php endif; ?>
                " role="alert">
            <?php echo $a->content; ?>
            <?php if($a->dismissible): ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            <?php endif; ?>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<?php if(isset($error)): ?>
    <div class="alert alert-danger
    <?php if($error->dismisible): ?> alert-dismissible fade show <?php endif; ?>
            " role="alert">
        <?php echo $error->content; ?>
        <?php if($error->dismisible): ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        <?php endif; ?>
    </div>
<?php endif; ?><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\DangerReal\resources\views/default/allerts.blade.php ENDPATH**/ ?>