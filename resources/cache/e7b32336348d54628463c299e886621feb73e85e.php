<div class="detailTable container-fluid">
    <div class="row">
        <div class="employeeList col-3">
            <div class="detail-row header">
                Nombres
            </div>
            <div id="detail-names-block">
                <div class="detail-row">
                    Juanito Gonzalez
                </div>
                <div class="detail-row">
                    Sergio Pérez
                </div>
                <div class="detail-row">
                    Juanito Gonzalez
                </div>
                <div class="detail-row">
                    Sergio Pérez
                </div>
            </div>
        </div>
        <div class="employeeData col-9">
            <div class="employeeDataContent">
                <div class="detail-row header hours row">
                    Horas
                </div>
                <div id="detail-data-block">
                    <div class="detail-row strype-dark">

                    </div>
                    <div class="detail-row strype-white">
                        <div style="
                    height: 40px;
                    width: 200px;
                    background-color: #1c7430;
                    border-radius: 4px;
                    "></div>
                        <div style="
                    height: 40px;
                    width: 200px;
                    background-color: #1c7430;
                    border-radius: 4px;
                    "></div>
                        <div style="
                    height: 40px;
                    width: 200px;
                    background-color: #1c7430;
                    border-radius: 4px;
                    "></div>
                    </div>
                    <div class="detail-row strype-dark">

                    </div>
                    <div class="detail-row strype-white">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/reportes/seccion/empleadosDetalle.blade.php ENDPATH**/ ?>