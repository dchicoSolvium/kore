<?php $__env->startPush('scripts'); ?>
    <link rel="stylesheet" href="/assets/style/style.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make("reportes.seccion.javascript", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->make("reportes.seccion.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <br>

    <div class="card card-container mx-auto">
        <a data-toggle="collapse" href="#collapse-detail" role="button" aria-expanded="false" aria-controls="collapseExample">
            <div class="card-header" id="headingOne">
                <span class="mx-auto">Detalle de empleados</span>
            </div>
        </a>
        <div class="collapse" id="collapse-detail">
            <?php echo $__env->make('reportes.seccion.empleadosDetalle', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>

    <div class="card card-container mx-auto">
        <a data-toggle="collapse" href="#collapse-sumary" role="button" aria-expanded="false" aria-controls="collapseExample">
            <div class="card-header" id="headingOne">
                <span class="mx-auto">Resúmenes</span>
            </div>
        </a>
        <div class="collapse" id="collapse-sumary">
            <?php echo $__env->make('reportes.seccion.graphs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make("default.default", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/reportes/reportes.blade.php ENDPATH**/ ?>