<?php $__env->startPush('scripts'); ?>
    <script type=“text/javascript”
            src=“https://cdnjs.cloudflare.com/ajax/libs/hideshowpassword/2.0.8/hideShowPassword.min.js”></script>

    <style>
        .focus input, .focus select {
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .focus label {
            font-weight: bold;
        }

        .password + .glyphicon {
            cursor: pointer;
            pointer-events: all;
        }

        iframe {
            width: 100%;
            height: 400px;
            border-style: none;
        }

        .custom-file-control .selected ::after {
            content: "" !important;
        }
    </style>

    <script>
        $(document).ready(function () {
            // toggle password visibility
            $('.password + .glyphicon').on('click', function () {
                $(this).toggleClass('glyphicon-eye-close').toggleClass('glyphicon-eye-open'); // toggle our classes for the eye icon

                if ($('.password').attr('type') == 'password') {
                    $('.password').attr('type', 'text');
                } else {
                    $('.password').attr('type', 'password');
                }

            });

            $('.custom-file-input').on('change', function () {
                var fileName = $(this).val();
                var label = $('label[for="' + $(this).attr('id') + '"]');
                label.html(fileName);
            })
        });
    </script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="container">
        <form id='form' method="post" action="parse" enctype="multipart/form-data">
            <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $f): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($f->kType->getName() == 'kId' ): ?>
                    <?php continue; ?>
                <?php endif; ?>
                <div class="form-group row
                    <?php if($f->focus): ?> focus <?php endif; ?>

                <?php if($f->kType->getName() == 'kPass'): ?>
                        has-feedback
                <?php endif; ?> "

                     <?php if($f->help): ?>
                     title="<?php echo e($f->help); ?>"
                        <?php endif; ?>
                >

                    <?php if($f->kType->getName() == 'kKidFrame'): ?>
                        <iframe class="embed-responsive-item" src="<?php echo e($f->link); ?>" width="100%" height="500px"
                                seamless=""></iframe></div>
                <?php continue; ?>
                <?php endif; ?>

                <?php if( $f->label == null ): ?>
                    <label $for="<?php echo e($f->getName()); ?>" class="col-sm-2 col-form-label"><?php echo e($f->getName()); ?>:</label>
                <?php else: ?>
                    <label $for="<?php echo e($f->getName()); ?>" class="col-sm-2 col-form-label"><?php echo e($f->label); ?>:</label>
                <?php endif; ?>

                <?php if($f->kType->getName() == 'kString' ): ?>
                    <input type="text" class="form-control col-sm-10"
                           name="<?php echo e($f->getName()); ?>" value="<?php echo e($entity->{$f->getName()}); ?>"
                           <?php if($f->disabled): ?> disabled <?php endif; ?>>
                <?php elseif($f->kType->getName() == 'kText' ): ?>
                    <textarea type="text" class="form-control col-sm-10"
                       name="<?php echo e($f->getName()); ?>"
                          <?php if($f->disabled): ?> disabled <?php endif; ?>><?php echo e($entity->{$f->getName()}); ?></textarea>
                <?php elseif($f->kType->getName() == 'kPass' ): ?>
                    <input type="password" class="form-control col-sm-10 password"
                           name="<?php echo e($f->getName()); ?>"
                           value="<?php echo e($entity->{$f->getName()}); ?>" <?php if($f->disabled): ?> disabled <?php endif; ?>>
                    <i class="glyphicon glyphicon-eye-open form-control-feedback" style="top: 0;"></i>
                <?php elseif($f->kType->getName() == 'kInt' ): ?>
                    <input type="number" class="form-control col-sm-10"
                           name="<?php echo e($f->getName()); ?>"
                           value="<?php echo e($entity->{$f->getName()}); ?>" <?php if($f->disabled): ?> disabled <?php endif; ?>>
                <?php elseif($f->kType->getName() == 'kFile' ): ?>
                    <div class="input-group col-sm-10" style="padding: 0px;">
                        <?php if($entity->{$f->getName()}): ?>
                            <div class="input-group-prepend">
                                <a class="btn btn-outline-secondary" href="/<?php echo e($entity->{$f->getName()}); ?>" download>
                                    Download
                                </a>
                            </div>
                        <?php endif; ?>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="<?php echo e($f->getName()); ?>"
                                   id="<?php echo e($f->getName()); ?>-id" aria-describedby="inputGroupFileAddon03">
                            <?php if($entity->{$f->getName()}): ?>
                                <label class="custom-file-label"
                                       for="<?php echo e($f->getName()); ?>-id"><?php echo e($entity->{$f->getName()}); ?></label>
                            <?php else: ?>
                                <label class="custom-file-label" for="<?php echo e($f->getName()); ?>-id">Choose file</label>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php elseif($f->kType->getName() == 'kTime' ): ?>
                    <input type="time" class="form-control col-sm-10"
                           name="<?php echo e($f->getName()); ?>"
                           value="<?php echo e(\kore\Kore::kTime()->unparse($entity->{$f->getName()})); ?>" <?php if($f->disabled): ?> disabled <?php endif; ?>>
                <?php elseif($f->kType->getName() == 'kBool' ): ?>
                    <input type="hidden" name="<?php echo e($f->getName()); ?>" value=false/>
                    <input type="checkbox" name="<?php echo e($f->getName()); ?>" value=true
                           <?php if( $entity->{$f->getName()} ): ?>
                           checked
                            <?php endif; ?> />
                <?php elseif($f->kType->getName() == 'kDate' ): ?>
                    <?php
                    if ($entity->{$f->getName()} == null) {
                        $entity->{$f->getName()} = new DateTime();
                    }
                    ?>
                    <input type="date" class="form-control col-sm-10"
                           name="<?php echo e($f->getName()); ?>"
                           value="<?php echo e($entity->{$f->getName()}->format('Y-m-d')); ?>" <?php if($f->disabled): ?> disabled <?php endif; ?>>
                <?php elseif($f->kType->getName() == 'kDatetime' ): ?>
                    <?php
                    if ($entity->{$f->getName()} == null) {
                        $entity->{$f->getName()} = new DateTime();
                    }
                    ?>
                    <input type="date" class="form-control col-sm-6"
                           name="<?php echo e($f->getName()); ?>"
                           value="<?php echo e($entity->{$f->getName()}->format('Y-m-d')); ?>" <?php if($f->disabled): ?> disabled <?php endif; ?>>
                    <span class="col-sm-1">A las</span>
                    <input type="number" class="form-control col-sm-1"
                           name="<?php echo e($f->getName()); ?>-hour" max="24" min="0"
                           value="<?php echo e($entity->{$f->getName()}->format('H')); ?>" <?php if($f->disabled): ?> disabled <?php endif; ?>>
                    <span class="col-sm-1">:</span>
                    <input type="number" class="form-control col-sm-1"
                           name="<?php echo e($f->getName()); ?>-minute" max="60" min="0"
                           value="<?php echo e($entity->{$f->getName()}->format('i')); ?>" <?php if($f->disabled): ?> disabled <?php endif; ?>>
                <?php elseif($f->kType->getName() == 'kSelect' || $f->kType->getName() == 'kFK' ): ?>
                    <select name="<?php echo e($f->getName()); ?>" class="form-control col-sm-10" <?php if($f->disabled): ?> disabled <?php endif; ?>>
                        <option value="">...</option>
                        <?php $__currentLoopData = $f->options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v => $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($v); ?>"
                                    <?php if($entity->{$f->getName()} == $v): ?> selected <?php endif; ?>><?php echo e($o); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                <?php elseif($f->kType->getName() == 'kColor' ): ?>
                    <input type="color" class="form-control col-sm-10"
                           name="<?php echo e($f->getName()); ?>" value="<?php echo e($entity->{$f->getName()}); ?>"
                           <?php if($f->disabled): ?> disabled <?php endif; ?>>
        <?php endif; ?>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>

    <input type="hidden" value=<?php echo e($id); ?> name=id>

    <input type="hidden" value=true name="formSend">

    <?php if(isset($nomenu)): ?>
        <input type="hidden" value=true name="nomenu">
    <?php endif; ?>

    <button type="submit" class="btn btn-primary float-right">Guardar</button>
    <button type="reset"
            onclick="location.href='<?php echo e(\kore\Kore::htmlRenderer()->createURL('table', -1, $model->getName())); ?>'"
            class="btn btn-secondary  float-right" style="margin-right: 2px;">Volver
    </button>
    </form>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make("default.default", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Khronos\resources\views/base/form.blade.php ENDPATH**/ ?>