<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Panel de administración Kore"/>
    <meta name="author" content="David Chico"/>

    <title>DangerReal</title>

    <link href="https://fonts.googleapis.com/css?family=Dosis&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <!-- Boostrap   -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <!-- Boostrap select from: https://developer.snapappointments.com/bootstrap-select/ -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script>

    <link rel="stylesheet" href="<?php echo e(\kore\Kore::getKore()->baseURL); ?>/assets/style/style.css">

    <style>
        .page-body-lgn{
            background-image: url("<?php echo e(\kore\Kore::getKore()->baseURL); ?>/assets/images/background/bg-lgn-1.jpg");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            background-attachment: fixed;
        }

    </style>
</head>
<body class="page-body-lgn">


<div class="page-container">
    
    
    

    <div class="main-content-lgn container">

        
        <form method="post">
            <div class="row">
                <span class="col-sm-10 center-block title">
                    <?php echo e(strtoupper(\kore\Kore::getKore()->appName)); ?>

                </span>
            </div>
            <div class="row">
                <input type="text" class="form-control col-sm-10 center-block" name="username"
                       style="margin-top: 10px; text-align: center;" placeholder="Identificador">
            </div>
            <div class="row">
                <input type="password" class="form-control col-sm-10 center-block" name="password"
                       style="margin-top: 10px; text-align: center;" placeholder="Password">
            </div>
            <div class="row">
                <input type="submit" class="btn btn-secondary col-sm-10 center-block"
                       style="margin-top: 10px; text-align: center;" value="Acceder">
            </div>
        </form>

        <br/>
    </div>

    <!-- Main Footer -->
    <!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
    <!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
    <!-- Or class "fixed" to  always fix the footer to the end of page -->
    <footer class="main-footer sticky footer-type-1">

        <div class="footer-inner">
            Copyright: “DangerReal® Copyright © 2019. Todos los derechos reservados”

        </div>

    </footer>


</div>

</body>
</html><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Naturgy\resources\views/auth/login.blade.php ENDPATH**/ ?>