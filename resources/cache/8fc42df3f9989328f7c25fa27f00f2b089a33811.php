<?php if(isset($menu)): ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <?php if(\kore\Kore::getKore()->appLogo): ?>
            <a class="navbar-brand" href="#">
                <div><img src="/<?php echo e(\kore\Kore::getKore()->appLogo); ?>" style="max-width: 100px;"></div>
            </a>
        <?php endif; ?>
        <a class="navbar-brand" href="#">
            <?php echo e(strtoupper(\kore\Kore::getKore()->appName)); ?>

        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(isset($m['submenu'])): ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo e($m['label']); ?>

                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <?php $__currentLoopData = $m['submenu']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a class="nav-link"
                                       href="<?php echo e($sm['link']); ?>"><?php echo e($sm['label']); ?></a>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </li>
                    <?php else: ?>
                        <li class="nav-item <?php if(isset($m['active']) && $m['active']): ?> active <?php endif; ?>">
                            <a class="nav-link"
                               href="<?php echo e($m['link']); ?>"><?php echo e($m['label']); ?></a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                
                    
                
                
                    
                
                
                    
                        
                    
                    
                        
                        
                        
                        
                    
                
                
                    
                
            </ul>
            <?php if($user): ?>
                <form class="navbar-text my-2 my-lg-0">
                    <div><?php echo e($user->username); ?></div>
                </form>
            <?php endif; ?>
        </div>
    </nav>
<?php endif; ?><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Kore\resources\views/default/headermenu.blade.php ENDPATH**/ ?>