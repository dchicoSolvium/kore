<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Panel de administración Kore"/>
    <meta name="author" content="David Chico"/>

    <title><?php echo e(\kore\Kore::getKore()->appName); ?></title>

    <link href="https://fonts.googleapis.com/css?family=Dosis&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <!-- Boostrap   -->



<!-- Boostrap select from: https://developer.snapappointments.com/bootstrap-select/ -->
    
    
    

    <?php if(isset($url)): ?>
        <script>
            window.history.pushState("", "", '<?php echo e($url); ?>');
        </script>
    <?php endif; ?>

    <?php echo $__env->yieldPushContent('scripts'); ?>

    <link rel="stylesheet" href="<?php echo e(\kore\Kore::getKore()->baseURL); ?>/assets/style/style.css">

    <style>
        .page-body{
            background-color: #efefef;
            background-image: url("<?php echo e(\kore\Kore::getKore()->baseURL); ?>/assets/images/background/bg-lgn-1.jpg");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            background-attachment: fixed;
        }
    </style>
</head>
<body class="page-body">


<div class="page-container">
    
    
    
    <?php echo $__env->make('default.headermenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="main-content mx-auto">

        

        <?php if(isset($parentPile) && count($parentPile) > 0): ?>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <?php $__currentLoopData = $parentPile; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="breadcrumb-item"><a href="<?php echo e($pp['url']); ?>"><?php echo e($pp['name']); ?></a></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ol>
            </nav>
        <?php endif; ?>


        <h1 class="text-center" id="title"><?php echo e($title); ?></h1>
        <br><br>


        <?php echo $__env->make('default.allerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->yieldContent('content'); ?>

        <br/>
        <!-- Main Footer -->
        <!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
        <!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
        <!-- Or class "fixed" to  always fix the footer to the end of page -->
        <?php if(isset($menu)): ?>
            <footer class="main-footer sticky footer-type-1">

                <div class="footer-inner">
                    Copyright: “DangerReal® Copyright © 2019. Todos los derechos reservados”

                </div>

            </footer>
        <?php endif; ?>
    </div>


</div>

</body>
</html><?php /**PATH C:\Users\david\Desktop\workspace\Desarrollo-Kore\Naturgy\resources\views/default/default.blade.php ENDPATH**/ ?>