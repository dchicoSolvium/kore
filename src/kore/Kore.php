<?php

namespace kore;

use kore\base\Entity;
use kore\base\KObject;
use kore\base\KRule;
use kore\base\utils\KAllert;
use mysql_xdevapi\Exception;

/**
 * Created by David Chico.
 * Mail: davidchico@solvium.es
 * Date: 26/03/2019
 */
class Kore extends KObject
{

    private static $self = null;

    private static $kore = null;

    public static $mask = [];

    public static $lang = null;

    public static $driver = null;

    public static $preprocessor = null;

    public static $renderer = null;

    public static $action = null;

    public static $kmodel = null;

    public static $target = [];

    private static $_target = [];

    public static $list = null;

    public static $params = [];


    const TREE_FIND_DELIMITER_FUNC_FORMAT = '_';
    const TREE_FIND_DELIMITER = '.';

    private static $config = [
        __DIR__ . '/../../config'
    ];


    public function __construct()
    {
        parent::__construct([], 'Kore');
    }


    /**
     * Función singueltón
     * @return Kore
     */
    static function getKore()
    {
        if (self::$kore == null) {
            self::$kore = new Kore();
        }

        return self::$kore;
    }


    /**
     * Permite insertar hijos y recuperarlos
     *
     * @param $name
     * @param $arguments
     */
    public static function __callStatic($name, $arguments)
    {
        $k = self::getKore();

//        if (is_callable($k->$name) || $k->hasMethod($name)){
//            return call_user_func([$k, $name], $arguments);
//        }

        // Kid
        if (count($arguments) == 0) {
            // Si no se incluyen argumentos, se busca en el Kore.
            // soporta busqueda por árbol o jerarquíca
            // al realizarse llamadas vía función, se usa el delimitador <<_>> en vez de <<.>>
            // ejemplo: $hijo = Kore::abuelo_padre_hijo();
            $name = str_replace(self::TREE_FIND_DELIMITER_FUNC_FORMAT,
                self::TREE_FIND_DELIMITER,
                $name);
            return Kore::getKore()->findTree($name);

        } else {
            $nname = preg_replace('/\\' . self::TREE_FIND_DELIMITER_FUNC_FORMAT
                . '[^\\' . self::TREE_FIND_DELIMITER_FUNC_FORMAT . ']+$/', '', $name);

            if ($nname == $name) {
                $kobject = self::getKore();
            } else {
                $nname = str_replace(self::TREE_FIND_DELIMITER_FUNC_FORMAT,
                    self::TREE_FIND_DELIMITER,
                    $nname);

                $name = preg_replace('/^[^.]+\\' . self::TREE_FIND_DELIMITER_FUNC_FORMAT . '/', '', $name);

                $kobject = self::findTree($nname);

                if ($kobject == null) {
                    throw new Exception();
                }
            }

            $kobject->$name = $arguments[0];
        }

    }

    /**
     * Permite acceder a los objetos cuando se llama a una función
     *
     * Es un truco un poco chapuza pero funcional
     *
     * Sirve para poder crear funcionalidades más complejas. No se recomienda su uso
     *
     * @param KObject|null $self
     * @return KObject|null
     */
    public static function kSelf($self = null)
    {

        if ($self == null) {
            return self::$self;
        }

        self::$self = $self;
    }

    public static function allert(KAllert $allert)
    {

        $c = count(self::getKore()->findByClass(KAllert::class));
        self::getKore()->{"allert_$c"} = $allert;
    }

    public static function addConfig($config)
    {
        self::$config[] = $config;
    }

    public static function getConfig()
    {
        return self::$config;
    }

    public static function getDriver()
    {
        $driver = self::$driver;
        if (!$driver){
            return null;
        }
        return self::$driver();
    }

    public static function getPreprocessor()
    {
        $preprocessor = self::$preprocessor;
        if (!$preprocessor){
            return null;
        }
        return self::$preprocessor();
    }

    public static function getKModel()
    {
        $kmodel = self::$kmodel;
        
        if (!$kmodel){
            return null;
        }
        return self::$kmodel();
    }

    public static function getAction()
    {
        $action = self::$action;
        if (!$action){
            return null;
        }
        return self::$action();
    }


    public static function getRenderer()
    {
        $renderer = self::$renderer;
        if (!$renderer){
            return null;
        }
        return self::$renderer();
    }


    public static function log($msg)
    {
        error_log($msg);
    }


    public static function addTarget(Entity $entity)
    {
        if (self::getTarget($entity->id, $entity->getModel())){
            self::$target[self::$_target[$entity->getModel()->getName()][$entity->id]] = $entity;
            return;
        }

        self::$target[] = $entity;
        self::$_target[$entity->getModel()->getName()][$entity->id] = count(self::$target) - 1;

        return;
    }


//    public static function indexTargets()
//    {
//        self::$_target = [];
//        $i = 0;
//
//        foreach (self::$target as $t) {
//            self::$_target[$t->getModel()->getName()][$t->id] = $i;
//            $i++;
//        }
//    }


    public static function getTarget($id, $model=null)
    {
        $modelName = "";

        if (!$model){
            if (!self::getKModel()){
                return null;
            }
            $modelName = self::getKModel()->getName();
        }else{
            $modelName = $model->getName();
        }

//        if (self::$_target == []) {
//            self::indexTargets();
//        }

        if (isset(self::$_target[$modelName][$id])) {
            return self::$target[self::$_target[$modelName][$id]];
        } else {
            return null;
        }
    }

    public static function checkTargets(&$entities)
    {
        $i = 0;

        foreach ($entities as $e) {
            $t = self::getTarget($e->id);

            if (!$t || $t->getModel() != $e->getModel()){
                continue;
            }

            if ($t != null) {
                $entities[$i] = $t;
            }

            if ($entities[$i]->delete) {
                unset($entities[$i]);
                array_values($entities);
            } else {
                $i++;
            }
        }

        return $entities;
    }

    public static function resetTargets()
    {
        self::$target = [];
        self::$_target = [];
    }

    public static function getTargets()
    {
        return self::$target;
    }

    public static function checkRules()
    {
        $rules = Kore::getKore()->findByClass(KRule::class, 1);

        $ret = true;

        // Comprobamos que todos los targets verifiquen sus reglas
        foreach (Kore::$target as $t) {
            if (!$t->validate()) {
                $ret = false;
            }
        }

        
        // Ordenamos las reglas por prioridades
        usort($rules, function ($a, $b) {
            if ($a->priority == $b->priority) {
                return 0;
            }
            return ($a->priority < $b->priority) ? -1 : 1;
        });

        if (!$rules) {
            return $ret;
        }

        // Verificamos las reglas
        foreach ($rules as $r) {
            if (!$r->execute()) {
                if ($r->message != null) {
                    Kore::allert(new KAllert(
                        $r->message,
                        'danger'
                    ));
                }
                $ret = false;
            }
        }

        return $ret;
    }

    /**
     * Búsqueda por árbol del formato:
     * abuelo.padre.hijo_...
     *
     * No es necesario buscar de esta forma, pero su uso hace las búsquedas más efectivas, por lo que
     * en caso de tener un árbol muy complejo, se recomienda su uso
     *
     * @param $name
     * @return KObject|Kore
     */
    private static function findTree($name)
    {
        $findTree = explode(self::TREE_FIND_DELIMITER, $name);
        $kobject = self::getKore();

        foreach ($findTree as $f) {
            $kobject = $kobject->find($f);

            if (is_array($kobject)) {
                foreach ($kobject as $f) {

                }
            }

        }

        return $kobject;
    }


}