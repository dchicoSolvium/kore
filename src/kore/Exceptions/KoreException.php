<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 18/04/2019
 * Time: 18:27
 */

namespace kore\Exceptions;


use Throwable;

class KoreException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}