<?php

namespace kore;

use kore\base\KObject;

/**
 * Created by David Chico.
 * Mail: davidchico@solvium.es
 * Date: 26/03/2019
 */
class KPlugin extends KObject
{

    public $config = [];

    public function install()
    {
    }


    /**
     * @return String[]
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return String[]
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

}