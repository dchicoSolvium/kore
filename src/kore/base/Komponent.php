<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


/**
 * Class Komponent
 * @package kore\base
 * @method preprocess
 * @method service
 * @method postprocess
 */
class Komponent extends KForceObject
{
    protected $model = [
        'methods' => [
            'preprocess',
            'postprocess'
        ],
        'properties' => [

        ],
        'kids' => [

        ]];

}