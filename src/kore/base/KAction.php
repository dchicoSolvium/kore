<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


/**
 * Class KAction
 * @package kore\base
 * @property mask
 */
class KAction extends KObject
{
    protected $model = [
        'methods' => [
            'execute'
        ],
        'properties' => [
        ],
        'kids' => [

        ]];
}