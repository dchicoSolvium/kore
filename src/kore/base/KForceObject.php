<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


/**
 * Class KForceObject
 * @package kore\base
 */
class KForceObject extends KObject
{

    function __construct(array $properties = [], $name = '')
    {
        parent::__construct($properties, $name);

        // Por defecto aplican su modelo para evitar errores
        $this->forceFit();
    }

    public function forceFit(){
        foreach ($this->model['methods'] as $m){
            if (!(isset($this->methods[$m]) && is_callable($this->methods[$m]))){
                $this->methods[$m] = function () { return true; };
            }
        }

        foreach ($this->model['properties'] as $p){
            if (!isset($this->properties[$p])){
                $this->properties[$p] = null;
            }
        }

        foreach ($this->model['kids'] as $p){
            if (!isset($this->kids[$p])){
                $this->kids[$p] = new KObject();
            }
        }
    }
}