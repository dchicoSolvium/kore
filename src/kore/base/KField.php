<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


use kore\Kore;
use mysql_xdevapi\Exception;

/**
 * Class KType
 * @package kore\base
 * @method validate
 * @property required
 * @property maxval
 * @property minval
 * @property required
 */
class KField extends KForceObject
{

    protected $model = [
        'methods' => [
            'validate'
        ],
        'properties' => [
            'required', // Required o no required
            'maxval',   // Valor máximo
            'minval',   // Valor mínimo
            'nullable', // puede o no ser nulo
            'default',  // valor por defecto
            'empty'     // Valor de vacío. Por defeto empty; para un string puede ser '', para un select -1...
        ],
        'kids' => [

        ]];

    /**
     * @var KType
     */
    public $kType;

    function __construct(array $properties = [], $kType = null, $name = '')
    {
        parent::__construct($properties, $name);

        if (is_string($kType)){
            $this->kType = Kore::$kType();

            if($this->kType == null){
                throw new \Exception();
            }

        }elseif ($kType instanceof KType){
            $this->kType = $kType;
        }else{
            throw new \Exception();
        }

    }

}