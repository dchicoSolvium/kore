<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


use mysql_xdevapi\Exception;

/**
 * Class KRule
 * @package kore\base
 * @method execute
 */
class KRule extends KObject
{

    const KINSUPDATE = 1;
    const KINSERT = 10;
    const KUPDATE = 100;
    const KDELETE = 1000;

    protected $model = [
        'methods' => [
            'execute'
        ],
        'properties' => [
            'message',
            'afectedFields',
            'event'
        ],
        'kids' => []];


    public function __construct($execute, $message = '', $afectedFields = [], $event = KRule::KINSUPDATE, array $properties = [], $name = '')
    {
        if (is_callable($execute)){
            $properties['execute'] = $execute;
            $properties['message'] = $message;
            $properties['afectedFields'] = $afectedFields;
            $properties['event'] = $event;

            parent::__construct($properties, $name);
        }else{
            throw new Exception();
        }
    }
}
