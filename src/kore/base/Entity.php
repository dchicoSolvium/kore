<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 11/04/2019
 * Time: 10:57
 */

namespace kore\base;


use kore\base\utils\KAllert;
use kore\Kore;

class Entity
{

    /**
     * @var KModel
     */
    private $model;

    /**
     * @var KField[]
     */
    private $fields;

    /**
     * @var
     */
    private $values;

    public $exists = false;

    public $modified = false;

    public $delete = false;


    public function __construct(KModel $model)
    {
        $this->model = $model;
        $this->fields = $this->model->findByClass(KField::class);
    }

    public function __get($name)
    {
        if ($name == 'id' && (!isset($this->values[$name]) || $this->values[$name] == -1)){
            $this->values[$name] = Kore::getDriver()->getNewId($this->model);
            return $this->values[$name];
        }

        if (!$this->hasField($name)){
            throw new \Exception("La propiedad $name no esta definida en " . $this->getModel()->getName());
        }

        if (! isset($this->values[$name])){
            return $this->model->$name->empty;
        }

        return $this->values[$name];
    }

    public function __set($name, $value)
    {
        if (!$this->hasField($name)){
            throw new \Exception("Propiedad $name no encontrada en " . $this->model->$name);
        }

        $this->values[$name] = $this->model->$name->kType->parse($value, $this->model->$name);
        $this->modified = true;
    }


    public function toString(){
        $mod = $this->getModel();
        $str = $mod->string;

        if ($str != ''){
            foreach ($this->getFields() as $f) {
                $val = $f->kType->toTableString($this->{$f->getName()}, $f);

                $str = str_replace("%" . $f->getName() . "%",
                    $val, $str);
            }
        }

        return $str;
    }

    public function jsonSerialize(){
        $data = [];
        $meta = [];

        $metaFields = \kore\Kore::metakomp()->metaFields;

        if (isset($metaFields)){
            foreach ($metaFields as $m){
                $meta[] = $m->getName();
            }
        }

        foreach ($this->getFields() as $f){
            if (!in_array($f->getName(), $meta)){
                $data[$f->getName()] = $this->{$f->getName()};
            }
        }
        
        return $data;
    }

    public function delete(){
        $this->delete = true;
    }

    public function getModel(){
        return $this->model;
    }

    /**
     * @return KField[]
     */
    public function getFields(){
        return $this->fields;
    }

    public function hasField($name){
        if ($this->model->$name != null and
            $this->model->$name instanceof KField){
            return true;
        }

        return false;
    }

    public function validate(){
        $ret = true;

        foreach ($this->fields as $f){
            $fname = $f->getName();
            $val = $this->$fname;

            if (!$f->kType->doValidate($val, $f)){
                var_dump($f->getName());
                var_dump($val); die();
                $f->focus = true;
                $ret = false;
            }
        }

        $rules = $this->model->findByClass(KRule::class);

        if (!$this->exists){
            $event = KRule::KINSERT;
        }elseif ($this->delete){
            $event = KRule::KDELETE;
        }elseif ($this->modified){
            $event = KRule::KUPDATE;
        }


        usort($rules, function ($a, $b){
            if ($a->priority == $b->priority) {
                return 0;
            }
            return ($a->priority < $b->priority) ? -1 : 1;
        });

        if (!$rules){
            return $ret;
        }

        foreach ($rules as $r){
            if (($r->event == KRule::KINSUPDATE && $event != KRule::KDELETE) ||
                $r->event == $event){
                if (!$r->execute($this)){
                    Kore::allert( new KAllert(
                        $r->message,
                        'danger'
                    ) );
                    $ret = false;
                    foreach ($r->afectedFields as $f){
                        $this->getModel()->$f->focus = true;
                    }
                }
            }
        }

        return $ret;
    }



}
