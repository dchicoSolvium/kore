<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 27/03/2019
 * Time: 11:32
 */

namespace kore\base;



use kore\Kore;

class KObject
{

    const SPACE = "  ";

    protected $model = ['methods' => [], 'properties' => [], 'kids' => []];

    protected $properties = [];

    protected $methods = [];

    /**
     * @var KObject[]
     */
    protected $kids = [];

    public $kname = '';

    /**
     * @var KObject | null
     */
    protected $kparent = null;

    public function __construct(array $properties = [], $name = '')
    {

        $this->setProperties($properties);

        if ($name != ''){
            $this->kname = $name;
        }
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->kname;
    }


    public function setName($name){
        $this->kname = $name;
    }


    public function getKParent(){
        return $this->kparent;
    }


    public function setKParent(KObject $kparent){
        $this->kparent = $kparent;
    }


    public function getNKids(){
        return count(array_keys($this->kids));
    }

    /**
     * Función que sirve para ver el árbol de objetos,
     * propiedades y métodos de este objeto y todos sus hijos
     *
     * @param int $level
     * @param bool $echo
     * @return string
     */
    public function dump($level = 0, $echo = true){
        $ini = str_repeat ( self::SPACE , $level);

        $str = '';
        if ($this->kname == ''){
            $str .= PHP_EOL;
        }else{
            $str .= "$this->kname =>" . PHP_EOL;
        }

        // Propiedades
        if (count(array_keys($this->properties)) > 0) {
            $str .= $ini . self::SPACE . "Properties =>" . PHP_EOL;

            foreach ($this->properties as $p => $v) {
                $str .= $ini . self::SPACE . self::SPACE . "$p => $v" . PHP_EOL;
            }
        }

        // Métodos
        if (count(array_keys($this->methods)) > 0) {
            $str .= $ini . self::SPACE . "Methods =>" . PHP_EOL;

            foreach ($this->methods as $p => $v) {
                $str .= $ini . self::SPACE . self::SPACE . "$p" . PHP_EOL;
            }
        }

        // Hijos
        if (count(array_keys($this->kids)) > 0) {
            $str .= $ini . self::SPACE . "Kids =>" . PHP_EOL;

            foreach ($this->kids as $p => $v) {
                $str .= $ini . self::SPACE . self::SPACE . $v->dump($level + 2, false) . PHP_EOL;
            }
        }

        if ($echo){
            echo $str;
        }
        return $str;

    }

    /**
     * Comprueba si el objeto se adapta al modelo definido
     * El modelo no es más que un array de métodos y propiedades
     *
     * @return bool
     */
    public function fitsModel(){
        foreach ($this->model['methods'] as $m){
            if (!(isset($this->methods[$m]) && is_callable($this->methods[$m]))){
                throw new \Exception('Validation error: KObject ' .
                    $this->getName() . ' does not fit its model'
                );
            }
        }

        foreach ($this->model['properties'] as $p){
            if (!isset($this->properties[$p])){
                throw new \Exception('Validation error: KObject ' .
                    $this->getName() . ' does not fit its model'
                );
            }
        }


        foreach ($this->model['kids'] as $k){
            if (!isset($this->kids[$k])){
                throw new \Exception('Validation error: KObject ' .
                    $this->getName() . ' does not fit its model'
                );
            }

            $p = $this->kids[$k];

            if (is_array($p)){
                foreach ($p as $k1){
                    if (!$k1->fitsModel()){
                        return false;
                    }
                }
            }else{
                if (!$p->fitsModel()){
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * Busqueda recursiva. Busca un KObject dentro del árbol de objetos.
     *
     * @param string $name
     * @return KObject
     */
    public function find(string $name, $depth = -1){

        if ($this->kname == $name){
            return $this;
        }

        if ($depth == 0){
            return [];
        }

        if (isset($this->kids[$name])){
            return $this->kids[$name];
        }

        $return = null;

        foreach ($this->kids as $k => $p){
            if (is_array($p)){
                foreach ($p as $p0){
                    $ret = $p0->find($name, $depth - 1);
                    $return = $ret ? $ret : $return;
                }
            }else{
                $ret = $p->find($name, $depth - 1);
                $return = $ret ? $ret : $return;
            }
        }

        return $return;
    }


    public function findByClass($class, $depth = -1){
        if ($this instanceof $class){
            return [$this];
        }

        if ($depth == 0){
            return [];
        }

        $return = [];

        foreach ($this->kids as $k => $p){
            if (is_array($p)){
                foreach ($p as $p0){
                    $ret = $p0->findByClass($class, $depth - 1);
                    $return = array_merge($ret, $return);
                }
            }else{
                $ret = $p->findByClass($class, $depth - 1);
                $return = array_merge($ret, $return);
            }
        }

        return $return;
    }

    public function findByClass_Names($class, $depth = -1){
        if ($this instanceof $class){
            return [$this->getName()];
        }

        if ($depth == 0){
            return [];
        }

        $return = [];

        foreach ($this->kids as $k => $p){
            if (is_array($p)){
                foreach ($p as $p0){
                    $ret = $p0->findByClass_Names($class, $depth - 1);
                    $return = array_merge($ret, $return);
                }
            }else{
                $ret = $p->findByClass_Names($class, $depth - 1);
                $return = array_merge($ret, $return);
            }
        }

        return $return;
    }

    public function getModel(){
        return $this->model;
    }

    public function setModel($model){
        if (is_array($model)){
            if (! isset($model['properties'])){
                $model['properties'] = [];
            }

            if (! isset($model['methods'])){
                $model['methods'] = [];
            }

            if (! isset($model['kids'])){
                $model['kids'] = [];
            }

            $this->model = $model;
        }
    }

    /**
     * Crea un duplicado del objeto y le asigna las nuevas propiedades
     * @param array $properties
     * @return KObject
     */
    public function extend(array $properties = [], $name = ''){
        $new = clone $this;

        $new->setProperties($properties);

        if ($name != ''){
            $new->setName($name);
        }else{
            $new->setName($this->getName() . 'Child');
        }

        return $new;
    }

    public function setProperties(array $properties){
        foreach ($properties as $k => $v){
            $this->$k = $v;
        }
    }

    public function hasMethod($name){
        if (isset($this->methods[$name])){
            return true;
        }
    }

    public function __get($name)
    {
        if (isset($this->properties[$name])){
            return $this->properties[$name];
        }elseif (isset($this->methods[$name])){
            return $this->methods[$name];
        }elseif (isset($this->kids[$name])){
            return $this->kids[$name];
        }
        return null;
    }


    public function __set($name, $value)
    {
        if ($value instanceof KObject){
            // Insertamos en un objeto si ya existe el objeto
            if (isset($this->kids[$name])){
                $parent = $this->kids[$name];
                $kname = $value->kname;

                if ($kname == ''){
                    $kname = $name . $parent->getNKids();

                }

                $parent->$kname = $value;
            }else{
                $value->setName($name);
                $this->kids[$name] = $value;
                $this->kids[$name]->setKParent($this);
            }
        }elseif (!is_callable($value)){
            $this->properties[$name] = $value;
        }else{
            $this->methods[$name] = $value;
        }
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if (!isset($this->methods[$name])) {
            throw new \Exception("Method $name not found");
        }

        $method = $this->methods[$name];

        if (!is_callable($method)) {
            throw new \Exception();
        }

        // de esta forma, desde la función se puede acceder a
        // kSelf, y se obtiene el objeto que realiza la llamada
        $lastKSelf = Kore::kSelf();
        Kore::kSelf($this);

        $ret = call_user_func_array($method, $arguments);

        // Recuperamos el último kself, permitiendo iterar
        Kore::kSelf($lastKSelf);

        return $ret;
    }
}