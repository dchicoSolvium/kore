<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


/**
 * Class KModel
 * @package kore\base
 * @method execute
 */
class KModel extends KObject
{

    public $tableFields = [];
    public $editFields = [];
    public $createFields = [];

    public $actions = [[
        'desc' => 'editar',
        'action' => 'edit'
    ], [
        'desc' => 'eliminar',
        'action' => 'delete'
    ]];

    public $string = '';


    public function getFields($invisible = false, $string = false){
        $fields = $this->findByClass(\kore\base\KField::class);

        if ($invisible){
            return $fields;
        }

        $ret = [];
        foreach ($fields as $f){
            if (!$f->invisible){
                if ($string){
                    $ret[] = $f->getName();
                }else{
                    $ret[] = $f;
                }
            }
        }

        return array_reverse ($ret);
    }

    public function getTableFields($string = false){
        if ($this->tableFields == []){
            return $this->getFields(false, $string);
        }

        if ($string){
            return $this->tableFields;
        }
        
        $ret = [];
        foreach ($this->tableFields as $tf){
            $ret[] = $this->{$tf};
        }

        return $ret;
    }

    public function getEditFields($string = false){
        if (count($this->editFields) == 0){
            return $this->getFields(false, $string);
        }

        if ($string){
            return $this->editFields;
        }

        $ret = [];
        foreach ($this->editFields as $tf){
            $ret[] = $this->{$tf};
        }

        return $ret;
    }

    public function getCreateFields($string = false){
        if (count($this->createFields) == 0){
            return $this->getFields(false, $string);
        }

        if ($string){
            return $this->createFields;
        }

        $ret = [];
        foreach ($this->createFields as $tf){
            $ret[] = $this->{$tf};
        }

        return $ret;
    }
}