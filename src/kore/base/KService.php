<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


/**
 * Class KService
 * @package kore\base
 * @method execute
 * @property event
 */
class KService extends KObject
{
    protected $model = [
        'methods' => [
            'execute'
        ],
        'properties' => [
        ],
        'kids' => []];
}