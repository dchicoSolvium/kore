<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


/**
 * Class KRenderer
 * @package kore\base
 */
class KRenderer extends KForceObject
{
    protected $model = [
        'methods' => [
            'default',
            'error'
        ],
        'properties' => [
        ],
        'kids' => [

        ]];
}