<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base\utils;


use kore\base\KObject;
use mysql_xdevapi\Exception;

/**
 * Class KAllert
 * @package kore\base
 */
class KAllert extends KObject
{
    protected $model = [
        'methods' => [
        ],
        'properties' => [
            'type',
            'content',
            'dismisible'
        ],
        'kids' => [

        ]];

    public function __construct($content, $type='warning', $dismisible = false, $name = '')
    {
        parent::__construct([], $name);
        /*
         * primary, secondary, success, danger, warning, info, light, dark
         */
        $this->type = $type;
        $this->content = $content;
        $this->dismisible = $dismisible;
    }

    public function jsonSerialize(){
        $data = [];

        $data["type"] = $this->type;
        $data["content"] = $this->content;
        $data["dismisible"] = $this->dismisible;

        return $data;
    }
}