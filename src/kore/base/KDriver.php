<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


/**
 * Class KAction
 * @package kore\base
 * @method init
 * @method close
 * @method setDDBB
 * @method unsetDDBB
 * @method find
 * @method getEntity
 * @method commit
 * @method delete
 * @method save
 * @method load
 */
class KDriver extends KForceObject
{
    protected $model = [
        'methods' => [
            'init',
            'close',
            'setDDBB',
            'unsetDDBB',
            'find',
            'commit',
            'delete',
            //'harddelete',
            'save',
            'load'
        ],
        'properties' => [
        ],
        'kids' => [

        ]];
}