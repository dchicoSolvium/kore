<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


/**
 * Class KPreprocess
 * @package kore\base
 * @method execute
 * @property event
 */
class KPreprocess extends KObject
{
    protected $model = [
        'methods' => [
            'execute'
        ],
        'properties' => [
        ],
        'kids' => []];
}