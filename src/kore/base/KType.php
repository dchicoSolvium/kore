<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 13:30
 */

namespace kore\base;


use kore\base\utils\KAllert;
use kore\Kore;

/**
 * Class KType
 * @package kore\base
 * @method parse
 * @method unparse
 * @method validate ($val, $kField)
 */
class KType extends KObject
{

    /**
     * Función útil, comprende las validaciones genéricas y comunes a todos los
     * tipos. Este tipo de validaciones se recomienda incluirlas aquí
     *
     * @param KType $type
     * @param $val
     * @return bool
     */
    function genericValidation($val, $kField){
        if (!$kField->kType->database_save){
            return true;
        }

        if ($kField->required){
            if ($val === null || $val === $this->empty){
                Kore::allert( new KAllert(
                    'Error validando ' . $kField->getName() . ' valor inválido: ' . $val .
                    '<br>El campo no puede estar vacío',
                    'danger'
                ) );
                return false;
            }
        }

        if ($kField->maxval != null){
            if ($val > $kField->maxval){
                Kore::allert( new KAllert(
                    'Error validando ' . $kField->getName() . ' valor inválido: ' . $val .
                    '<br>El campo no puede ser mayor que ' . $kField->maxval,
                    'danger'
                ) );
                return false;
            }
        }

        if ($kField->minval !== null){
            if ($val < $kField->minval){
                Kore::allert( new KAllert(
                    'Error validando ' . $kField->getName() . ' valor inválido: ' . $val .
                    '<br>El campo no puede ser menor que ' . $kField->minval,
                    'danger'
                ) );
                return false;
            }
        }

        return true;
    }


    function doValidate($val, $kField){
        $validation = true;
        $validation = $validation && $this->genericValidation($val, $kField);
        $validation = $validation && $this->validate($val, $kField);
        $validation = $validation && $kField->validate($val, $kField);
        return $validation;
    }


    protected $model = [
        'methods' => [
            'parse',
            'unparse',
            'validate'
        ],
        'properties' => [

        ],
        'kids' => [

        ]];

    public function __construct(array $properties = [], $name = '')
    {
        parent::__construct($properties, $name);
    }
}