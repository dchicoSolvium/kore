<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 09/04/2019
 * Time: 17:05
 */

namespace kore;


use kore\base\Entity;
use kore\base\KAction;
use kore\base\KModel;
use kore\base\KObject;
use kore\base\Komponent;
use kore\base\KPreprocess;
use kore\base\KService;
use kore\base\utils\KAllert;
use kore\Exceptions\KoreException;

class KEngine
{

    private static $plugins = [
    ];


    static function play(){

        try{

            // Fase 1: Se ejecutan todos los archivos de configuración definidos en el Kore
            // Los directorios se exploran de forma completa. En caso de que se indique
            // un directorio, se ejecutaran todos los archivos de configuración
            // de los directorios y subdirectorios
            self::loadConfig();
    
            Kore::getDriver()->init();

            // Fase 2: Ejecuta todos los preprocesos. En este punto se deberá identificar
            // la acción que se deber realizar
            self::preprocess();
            
            // Fase 3: Se ejecutan los pocedimientos adecuados para realizar la acción
            // concreta
            self::executeAction();

            // Fase 4: Se ejecutan todos los postprocess adecuados. Esto incluye servicios
            // de comprobación
            self::services();

            // Fase 5: Commit de la DDBB
            self::commit();

            // Fase 6:
            self::render();

            Kore::getDriver()->close();

        }catch (\Exception $e){
                var_dump($e);
                var_dump($e->getMessage());
                var_dump($e->getLine());
                var_dump($e->getFile());
                var_dump($e->getTraceAsString());
                echo $e->getTraceAsString();
            //Kore::getRenderer()->error($e);
        }
    }


    public static function reAction($action, $model = null){
        Kore::$action = $action;

        if ($model != null){
            Kore::$kmodel = $model;
        }

        Kore::resetTargets();

        try{
            // Fase 3: Se ejecutan los pocedimientos adecuados para realizar la acción
            // concreta
            self::executeAction();

            // Fase 4: Se ejecutan todos los postprocess adecuados. Esto incluye servicios
            // de comprobación
            self::services();

            // Fase 5: Commit de la DDBB
            self::commit();

            // Fase 6:
            self::render();

            Kore::getDriver()->close();

        }catch (\Exception $e){
            Kore::getRenderer()->error($e);
        }
    }

    /**
     * Función singueltón
     * @return Kore
     */
    static function registerPlugin(KPlugin $plugin)
    {
        self::$plugins[] = $plugin;

        foreach ($plugin->getConfig() as $c){
            Kore::addConfig($c);
        }

        $plugin->install();
    }


    private static function loadConfigDir($dir){
        $files = scandir($dir);

        foreach ($files as $f){
            if ($f == '.' || $f == '..'){
                continue;
            }

            $fdir = "$dir/$f";
            if (is_file($fdir)){
                if (preg_match('/\.php$/', $fdir))
                    include_once $fdir;

            }elseif (is_dir($fdir)){
                self::loadConfigDir($fdir);
            }
        }
    }

    static function loadConfig(){
        $config = Kore::getConfig();
        $plugConfig = [];

        foreach ($config as $c){
            if (is_file($c)){
                if (preg_match('/\.php$/', $c))
                    include_once $c;

            }elseif (is_dir($c)){
                self::loadConfigDir($c);
            }
        }


        // Check models
        if (!Kore::getKore()->fitsModel()){
            throw new \Exception('Validation error: KObject Kore' .
                ' does not fit its model'
            );
        }


    }

    private static function preprocess(){
        $mainPpr = Kore::getPreprocessor();
        $komponent = Kore::getKore()->findByClass(Komponent::class);
        $preprocess = Kore::getKore()->findByClass(KPreprocess::class);
        $mask = Kore::getKore()->mask;

        if ($mainPpr instanceof KPreprocess){
            $mainPpr->execute(Kore::getKore());
        }

//        foreach ($preprocess as $p){
//            if (count($mask) > 0 && in_array($p->getName(), $mask)){
//                $p->execute(Kore::getKore());
//            }
//        }

        $komponent = array_reverse($komponent);

        foreach ($komponent as $k){
            if (!(is_array($mask) > 0 && in_array($k->getName(), $mask))){
                $k->preprocess(Kore::getKore());
            }
        }
    }

    private static function executeAction(){
        $action = Kore::getAction();

        $action->execute(Kore::getKore());
    }

    private static function services(){
        $services = Kore::getKore()->findByClass(KService::class);
        $komponent = Kore::getKore()->findByClass(Komponent::class);
        $mask = Kore::getKore()->mask;

        foreach ($komponent as $k){
            if (!(is_array($mask) > 0 && in_array($k->getName(), $mask)) ||
                count($mask) == 0){
                $k->postprocess(Kore::getKore());
            }
        }


        foreach ($services as $p){
            if (!(is_array($mask) > 0 && !in_array($p->getName(), $mask))){
                $p->execute(Kore::getKore());
            }
        }


    }

    private static function commit(){
        Kore::getKore()->hascommit = false;

        if (Kore::$target != null){
            if (Kore::checkRules()){
                Kore::getKore()->hascommit = true;
                Kore::getDriver()->commit(Kore::$target);
            }
        }

        $komponent = Kore::getKore()->findByClass(Komponent::class);
        $mask = Kore::getKore()->mask;

        foreach ($komponent as $k){
            if (is_callable($k->postcommit)){
                if (!(count($mask) > 0 && in_array($k->getName(), $mask)) ||
                    count($mask) == 0){
                    $k->postcommit(Kore::getKore());
                }
            }
        }

        return;
    }

    private static function render(){
        $render = Kore::getRenderer();
        $action = Kore::$action;

        if (is_callable($render->$action)){
            $render->$action();
        }else{
            $render->default();
        }

    }
}
