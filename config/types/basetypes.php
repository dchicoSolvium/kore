<?php


use kore\Kore;
use kore\base\KType;
use kore\base\KTree;
use kore\base\utils\KAllert;


// Tipo de objeto básico, del que extenderán el resto de tipos de objeto
Kore::baseType(new KType([
    // Funciones básicas de los types
    'parse' => function ($val) {
        return $val;
    },
    'unparse' => function ($val) {
        return $val;
    },
    // Validaciones genéricas
    'validate' => function ($val, $field) {
        return Kore::kSelf()->genericValidation($val, $field);
    },
    'empty' => null,
    'database_save' => true,
    'MySQL_Insert' => function ($val) {
        if (is_null($val)) {
            return 'NULL';
        }
        return $val;
    },
    'MySQL_Get' => function ($val) {
        return $val;
    },
    'toTableString' => function ($val, $field) {
        return $val;
    }
]));


// Tipo kInt: Números enteros
Kore::kInt(Kore::baseType()->extend([
    'parse' => function ($val) {
        return intval($val);
    },
    'validate' => function ($val) {
        if (is_string($val)) {
            if ($val == '-1') {
                return true;
            }
            if (!preg_match('/^[0-9]+$/', $val)) {
                return false;
            }
        } elseif (!is_int($val)) {
            return false;
        }

        return true;
    },
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . " INT(6)";

        return $sql;
    }
]));


// Tipo kFloat: Números con coma flotante
Kore::kFloat(Kore::baseType()->extend([
    'parse' => function ($val) {
        return floatval($val);
    },
    'validate' => function ($val) {
        if (is_string($val)) {
            if ($val == '-1') {
                return true;
            }
            if (preg_match('/^[0-9]+\.[0-9]+$/', $val)) {
                return true;
            }elseif (!preg_match('/^[0-9]+$/', $val)){
                return false;
            }
        } elseif (!is_float($val)) {
            return false;
        }

        return true;
    },
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . " float";

        return $sql;
    }
]));


Kore::kTime(Kore::kInt()->extend([
    'parse' => function ($val) {
        if (is_string($val)) {
            $data = explode(':', $val);

            if (!isset( $data[1])){
                return $data[0];
            }

            $mins = intval($data[0]) * 60 + intval($data[1]);
            return $mins;
        }

        return intval($val);
    },
    'unparse' => function ($val) {
        $h = floor($val/60);
        $m = $val%60;

        if ($h < 10){
            $h = '0' . $h;
        }
        if ($m < 10){
            $m = '0' . $m;
        }

        return $h . ':' . $m;
    },
    'toTableString' => function ($val, $field) {
        return Kore::kSelf()->unparse($val);
    }
]));

Kore::kId(Kore::kInt()->extend([
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . "  INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY";

        return $sql;
    }
]));

Kore::kString(Kore::baseType()->extend([
    'parse' => function ($val) {
        return $val;
    },
    'empty' => '',
    'validate' => function ($val) {
        if (!is_string($val) && $val != null) {
            return false;
        }

        return true;
    },
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . " VARCHAR(200) CHARACTER SET utf8";

        return $sql;
    },
    'MySQL_Insert' => function ($val) {
        return "'$val'";
    }
]));

Kore::kText(Kore::baseType()->extend([
    'parse' => function ($val) {
        return $val;
    },
    'validate' => function ($val) {
        if (is_string($val) || $val == null) {
            return true;
        }

        return false;
    },
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . " TEXT CHARACTER SET utf8";

        return $sql;
    },
    'MySQL_Insert' => function ($val) {
        return "'$val'";
    }
]));

Kore::kBool(Kore::baseType()->extend([
    'parse' => function ($val) {
        if (is_string($val)) {
            if ($val == 'true') {
                return true;
            } else {
                return false;
            }
        } else {
            if ($val) {
                return true;
            } else {
                return false;
            }
        }
    },
    'validate' => function ($val) {
        return true;
    },
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . " BOOL";

        return $sql;
    },
    'MySQL_Insert' => function ($val) {
        if ($val) {
            return 1;
        } else {
            return 0;
        }
    },
    'MySQL_Get' => function ($val) {
        if ($val == 1) {
            return true;
        } else {
            return false;
        }
    }
]));

Kore::kDate(Kore::baseType()->extend([
    'parse' => function ($val) {
        if ($val instanceof DateTime) {
            return $val;
        }
        return new DateTime($val);
    },
    'validate' => function ($val) {
        return true;
    },
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . " TIMESTAMP";

        if ($field->default == 'NOW' ||
            $field->default == 'CURRENT_TIMESTAMP') {

            $sql .= ' DEFAULT CURRENT_TIMESTAMP';
        }

        return $sql;
    },
    'MySQL_Insert' => function ($val) {
        return "'" . $val->format('Y-m-d') . " 00:00:00'";
    },
    'MySQL_Get' => function ($val) {
        return new DateTime($val);
    },
    'toTableString' => function ($val, $field) {
        return $val->format('Y-m-d');
    }
]));

Kore::kDatetime(Kore::baseType()->extend([
    'parse' => function ($val, \kore\base\KField $field) {
        if ($val == null) {
            return null;
        }

        if ($val instanceof DateTime) {
            return $val;
        }

        if (is_array($val) && isset($val['date'])){
            $val = $val['date'];
        }

        $val = new DateTime($val);

        $h = 0;
        $m = 0;

        if (isset(Kore::$params[$field->getName() . '-hour'])) {
            $h = intval(Kore::$params[$field->getName() . '-hour']);
        }

        if (isset(Kore::$params[$field->getName() . '-minute'])) {
            $m = intval(Kore::$params[$field->getName() . '-minute']);
        }

        $val->setTime($h, $m);

        return $val;
    },
    'validate' => function ($val) {
        return true;
    },
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . " TIMESTAMP";

        if ($field->default == 'NOW' ||
            $field->default == 'CURRENT_TIMESTAMP') {

            $sql .= ' DEFAULT CURRENT_TIMESTAMP';
        }

        return $sql;
    },
    'MySQL_Insert' => function ($val) {
        if ($val){
            return "'" . $val->format('Y-m-d H:i:s') . "'";
        }
        return 'NULL';
    },
    'MySQL_Get' => function ($val) {
        if ($val){
            return new DateTime($val);
        }
        return null;
    },
    'toTableString' => function ($val, $field) {
        if ($val == null) {
            return '';
        }
        return $val->format('Y-m-d H:i:s');
    }
]));

Kore::kSelect(Kore::baseType()->extend([
    'parse' => function ($val) {
        if (!$val) {
            return '';
        }
        return $val;
    },
    'validate' => function ($val) {
        return true;
    },
    'empty' => '',
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . " TEXT CHARACTER SET utf8";

        return $sql;
    },
    'MySQL_Insert' => function ($val) {
        return "'$val'";
    },
    'toTableString' => function ($val, $field) {
        if ($val) {
            return $field->options[$val];
        }
        return '';
    }
]));


Kore::kFile(Kore::baseType()->extend([
    'uploadDir' => 'uploads',
    'parse' => function ($val, \kore\base\KField $field) {
        if (!isset($_FILES[$field->getName()])) {
            return $val;
        }


        if (isset($_FILES[$field->getName()]['kAlreadyUploaded'])) {
            return $val;
        }

        $f = $_FILES[$field->getName()];

        if ($f["error"] == UPLOAD_ERR_NO_FILE) {
            if ($field->required) {
                Kore::allert(new KAllert(
                    'No se ha seleccionado ningún fichero para ' . $field->getName(),
                    'danger'
                ));
            }

            return $val;
        } else {
            if ($f["error"] != UPLOAD_ERR_OK) {
                Kore::allert(new KAllert(
                    'Error subiendo archivo 1' . $field->getName(),
                    'danger'
                ));
                return '';
            }
        }

        $pathname = Kore::kFile()->uploadDir . '/' . $field->getKParent()->getName() . '/' . date("D-M-d") . '/';

        $file = $pathname . $f['name'];

        $tmp_name = $f["tmp_name"];

        if (!is_dir($pathname)) {
            mkdir($pathname, 0777, true);
        }

        move_uploaded_file($tmp_name, $file);

        $_FILES[$field->getName()]['kAlreadyUploaded'] = true;

        return $file;
    },
    'empty' => '',
    'validate' => function ($val, \kore\base\KField $field) {
        if ($val == '') {
//            if ($field->required){
//                Kore::allert( new KAllert(
//                    'Error validando ' . $kField->getName() . ' valor inválido: ' . $val .
//                    '<br>El campo no puede estar vacío',
//                    'danger'
//                ) );
//                return false;
//            }
            return true;
        }


        if (!is_string($val)) {
            Kore::allert(new KAllert(
                'Error subiendo archivo 2' . $field->getName(),
                'danger'
            ));
            return false;
        }


        if (isset($field->extensions)) {
            $extension = explode('.', $_FILES[$field->getName()])[1];

            if (!in_array($extension, $field->extensions)) {
                Kore::allert(new KAllert(
                    'Error subiendo archivo 3' . $field->getName(),
                    'danger'
                ));
                return false;
            }

            return true;
        }

        return true;
    },
    'MySQL_Create' => function ($driver, $field) {
        $pref = $driver->propertyPrefix;

        $sql = $pref . $field->getName() . " VARCHAR(200) CHARACTER SET utf8";

        return $sql;
    },
    'MySQL_Insert' => function ($val) {
        return "'$val'";
    }
]));


Kore::kColor(Kore::kString()->extend());
