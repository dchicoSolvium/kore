<?php


use kore\Kore;
use kore\base\KType;
use kore\base\KTree;


// Tipo de objeto básico, del que extenderán el resto de tipos de objeto
Kore::kFK(Kore::kInt()->extend([
    // Funciones básicas de los types
    'parse' => function ($val) {
        if (!$val){
            return -1;
        }
        return intval($val);
    },
    'unparse' => function ($val) {
        return $val;
    },
    // Validaciones genéricas
    'validate' => function ($val, $field) {
        $driver = Kore::getDriver();
        $target = $field->target;

        if (!$val || $val == -1){
            if ($field->nullable || !$field->required){
                return true;
            }else{
                Kore::allert(new \kore\base\utils\KAllert(
                    "El campo ". $field->getName() ." no puede estar vacío",
                    'danger'
                ));
                return false;
            }
        }

        if (!$target) {
            throw new Exception();
        } elseif (is_string($target)) {
            $target = Kore::{$target}();
        } elseif (!$target instanceof \kore\base\KModel) {
            throw new Exception();
        }

        $obj = $driver->getEntity($target, $val);

        if ($obj || ($field->nullable || !$field->required)){
            return true;
        }

        Kore::allert(new \kore\base\utils\KAllert(
            "La entidad definida en " . $field->getName() .
            " no existe",
            'danger'
        ));
        die();

        return false;
    },
    'empty' => -1,
    'MySQL_Insert' => function ($val) {
        return $val;
    },
    'MySQL_Get' => function ($val) {
        return $val;
    },
    'toTableString' => function ($val, $field) {
        if($val && $val >= 0){
            if (isset($field->options[$val])){
                return $field->options[$val];
            }
            return '';
        }
        return '';
    },
    'getParent' => function ($entity, $field) {
        $driver = Kore::getDriver();
        $target = $entity->getModel()->$field->target;

        if (!$target) {
            throw new Exception();
        } elseif (is_string($target)) {
            $target = Kore::{$target}();
        } elseif (!$target instanceof \kore\base\KModel) {
            throw new Exception();
        }

        return $driver->getEntity($target, $entity->$field);
    }
]));



Kore::kFK()->renderString = function ($obj, $field){
    $str = $field->string;

    foreach ($obj->getFields() as $f2) {
        $val = $f2->kType->toTableString($obj->{$f2->getName()}, $f2);

        $str = str_replace("%" . $f2->getName() . "%",
            $val, $str);
    }

    return $str;
};

Kore::kFK()->parentField = function ($target, \kore\base\KModel $model){

    if ($target instanceof \kore\base\KModel) {
        $target = $target->getName();
    } elseif (!is_string($target)) {
        throw new Exception();
    }

    foreach ($model->findByClass(\kore\base\KField::class) as $f){
        if ($f->kType->getName() == 'kFK' && $f->target == $target){
            return $f;
        }
    }

    return null;
};


Kore::kFKRender(new \kore\base\KService([
    'execute' => function () {
        $driver = Kore::getDriver();
        $model = Kore::getKModel();

        if (!$model || $model == ''){
            return;
        }

        $fields = $model->findByClass(\kore\base\KField::class);

        foreach ($fields as $f) {
            if ($f->kType->getName() == 'kFK') {
                $target = $f->target;

                if (!$target) {
                    throw new Exception();
                } elseif (is_string($target)) {
                    $target = Kore::{$target}();
                } elseif (!$target instanceof \kore\base\KModel) {
                    throw new Exception();
                }

                $filters = [];
                $filters2 = [];

                // Filtramos también por parent Piles
                if (isset(\kore\Kore::$params['parentPile'])){
                    foreach (\kore\Kore::$params['parentPile'] as $p){
                        $f2 = \kore\Kore::kFK()->parentField($p['model'], $target);

                        if ($f2){
                            $filters[$f2->getName()] = $p['id'];
                            // los -1 se consideran opciones genéricas
                            $filters2[$f2->getName()] = -1;
                        }
                    }
                }

                if (count(array_keys($filters)) > 0){
                    // if field == parentId || -1
                    $list = $driver->list($target, $filters);
                    $list = array_merge($driver->list($target, $filters2), $list);
                }else{
                    // Si no se utilizan todos
                    $list = $driver->list($target);
                }

                if ($target->orderBy){
                    $GLOBALS['orderBy'] = $target->orderBy;

                    usort($list, function ($a, $b){
                        return strcmp($b->{$GLOBALS['orderBy']}, $a->{$GLOBALS['orderBy']});
                    });
                }


                $options = [];

                foreach ($list as $l) {
                    $options[$l->id] = Kore::kFK()->renderString($l, $f);
                }

                $f->options = $options;
            }
        }

    }
]));


Kore::kFKDeleteCheck(new \kore\base\KRule(function () {
    $driver = Kore::getDriver();
    $targets = Kore::getTargets();

    foreach ($targets as $t){
        $model = $t->getModel();
        $id = $t->id;

        if (!$t->delete){
            continue;
        }

        foreach (Kore::getKore()->findByClass(\kore\base\KModel::class) as $m){
            foreach ($m->findByClass(\kore\base\KField::class) as $f){
                if ($f->kType->getName() == 'kFK' && $f->target == $model->getName()){
                    $list = $driver->list($m, [$f->getName() => $id]);

                    if (count($list) > 0){
                        Kore::allert(new \kore\base\utils\KAllert(
                            "Existe una referencia a la entidad ". $model->getName() .
                            " con id $id en " . $m->getName(),
                            'danger'
                        ));
                        return false;
                    }

                }
            }
        }

    }

    return true;

}, null));




Kore::kKidFrame(Kore::baseType()->extend([
    'database_save' => false
]));


\kore\Kore::kKidFrame()->createURL = function ($id, $model, $child, $action="table"){
    $model = \kore\Kore::$kmodel;

    $pile = '';
    if (isset(\kore\Kore::$params['parentPile'])){
        foreach (\kore\Kore::$params['parentPile'] as $p){
            $pile .= "/" . $p["model"] . "/" . $p["id"];
        }
    }

    return $pile . "/$model/$id/$child/$action?nomenu=true";
};


Kore::kKidFrameURLRender(new \kore\base\KService([
    'execute' => function () {
        $model = Kore::getKModel();
        
        if (!isset(\kore\Kore::$params['id'])){
            return;
        }
        
        $id = \kore\Kore::$params['id'];


        if (!$model || $model == ''){
            return;
        }

        $fields = $model->findByClass(\kore\base\KField::class);


        foreach ($fields as $f) {
            if ($f->kType->getName() == 'kKidFrame') {
                $f->link = \kore\Kore::kKidFrame()->createURL($id, $model->getName(), $f->target);
            }
        }

    }
]));