<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */


\kore\Kore::setup(new \kore\base\KAction([
    'execute' => function () {
        \kore\Kore::getKore()->setModel(null);
        
        $driver = \kore\Kore::getDriver();

        $driver->setDDBB();

        return;
    }]));


\kore\Kore::drop(new \kore\base\KAction([
    'execute' => function () {
        \kore\Kore::getKore()->setModel(null);

        $driver = \kore\Kore::getDriver();

        $driver->unsetDDBB();

        return;
    }]));


\kore\Kore::reset(new \kore\base\KAction([
    'execute' => function () {
        \kore\Kore::getKore()->setModel(null);

        $driver = \kore\Kore::getDriver();

        $driver->unsetDDBB();

        $driver->setDDBB();

        return;
    }]));


\kore\Kore::backup(new \kore\base\KAction([
    'execute' => function () {
        global $argv;

        \kore\Kore::getKore()->setModel(null);

        $driver = \kore\Kore::getDriver();

        $file = $argv[2];

        $driver->save($file);

        return;
    }]));


\kore\Kore::restore(new \kore\base\KAction([
    'execute' => function () {
        global $argv;

        \kore\Kore::getKore()->setModel(null);

        $driver = \kore\Kore::getDriver();

        $file = $argv[2];

        $driver->load($file);

        \kore\Kore::getKore()->forceParams = true;

        return;
    }]));
