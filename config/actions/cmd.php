<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */


\kore\Kore::cinsert(new \kore\base\KAction([
    'execute' => function () {
        $entity = new \kore\base\Entity(\kore\Kore::getKModel());

        $stdin = fopen ("php://stdin","r");

        $fields = $entity->getFields();

        echo "Creando una entidad del modelo " . $entity->getModel()->getName() . ":" . PHP_EOL;

        foreach ($fields as $f){
            $fname = $f->getName();

            if ($fname == 'id'){
                continue;
            }

            echo $fname . ': ';
            $val = trim(fgets($stdin));
            $entity->$fname = $val;
        }

        \kore\Kore::$target = $entity;

        return;
    }]));

\kore\Kore::cdelete(new \kore\base\KAction([
    'execute' => function () {

        $stdin = fopen ("php://stdin","r");

        echo 'Inserta el ID del ' . \kore\Kore::getKModel()->getName() . ' a eliminar: '.PHP_EOL;
        $id = intval(trim(fgets($stdin)));

        $entity = \kore\Kore::getDriver()->getEntity(\kore\Kore::getKModel(), $id);

        $entity->delete();

        \kore\Kore::addTarget($entity);

        return;
    }]));


\kore\Kore::clist(new \kore\base\KAction([
    'execute' => function () {
        $entity = new \kore\base\Entity(\kore\Kore::getKModel());

        $driver = \kore\Kore::getDriver();

        \kore\Kore::$list = $driver->list(\kore\Kore::getKModel());

        return;
    }]));


