<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */


\kore\Kore::parse(new \kore\base\KAction([
    'execute' => function () {
        if (\kore\Kore::$params['id'] != -1){
            $entity = \kore\Kore::getDriver()->getEntity(
                \kore\Kore::getKModel(),
                \kore\Kore::$params['id']
            );
        }else{
            $entity = new \kore\base\Entity(\kore\Kore::getKModel());
        }

        \kore\Kore::$params['entity'] = $entity;

        $fields = $entity->getFields();

        foreach ($fields as $f){
            $fname = $f->getName();

            if (isset(\kore\Kore::$params[$fname])){
                $entity->$fname = \kore\Kore::$params[$fname];
            }elseif ($entity->$fname == null){
                $entity->$fname = $f->default;
            }elseif ($f->kType->getName() == 'kBool'){
                $entity->$fname = false;
            }
        }

        if (isset(\kore\Kore::$params['parentPile'])){
            foreach (\kore\Kore::$params['parentPile'] as $p){
                $f = \kore\Kore::kFK()->parentField($p['model'], \kore\Kore::getKModel());
                if ($f){
                    $entity->{$f->getName()} = $p['id'];
                }
            }
        }

        \kore\Kore::addTarget($entity);
        
        return;
    }]));

\kore\Kore::delete(new \kore\base\KAction([
    'execute' => function () {

        $id = \kore\Kore::$params['id'];

        $entity = \kore\Kore::getDriver()->getEntity(\kore\Kore::getKModel(), $id);

        $entity->delete();

        \kore\Kore::table()->execute();

        \kore\Kore::addTarget($entity);

        return;
    }]));


\kore\Kore::table(new \kore\base\KAction([
    'execute' => function () {
        $kore = \kore\Kore::getKore();

        $driver = \kore\Kore::getDriver();

        if (isset(\kore\Kore::$params['filters'])){
            $filters = json_decode(urldecode(\kore\Kore::$params['filters']), true);
        }else{
            $filters = [];
        }

        if (isset(\kore\Kore::$params['parentPile'])){
            foreach (\kore\Kore::$params['parentPile'] as $p){
                $f = \kore\Kore::kFK()->parentField($p['model'], \kore\Kore::getKModel());
                if ($f){
                    $filters[$f->getName()] = $p['id'];
                }
            }
        }

        \kore\Kore::$list = $driver->list(\kore\Kore::getKModel(), $filters);
        
        return;
    }]));


\kore\Kore::create(new \kore\base\KAction([
    'execute' => function () {
        return;
    }]));


\kore\Kore::edit(new \kore\base\KAction([
    'execute' => function () {
        $entity = new \kore\base\Entity(\kore\Kore::getKModel());

        $driver = \kore\Kore::getDriver();

        $driver->list(\kore\Kore::getKModel());

        return;
    }]));


