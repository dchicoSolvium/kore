<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */


\kore\Kore::list(new \kore\base\KAction([
    'execute' => function () {
        $kore = \kore\Kore::getKore();

        $driver = \kore\Kore::getDriver();

        $filters = [];

        if (isset(\kore\Kore::$params['parentPile'])){
            foreach (\kore\Kore::$params['parentPile'] as $p){
                $f = \kore\Kore::kFK()->parentField($p['model'], \kore\Kore::getKModel());
                if ($f){
                    $filters[$f->getName()] = $p['id'];
                }
            }
        }

        \kore\Kore::$list = $driver->list(\kore\Kore::getKModel(), $filters);

        return;

    }]));


\kore\Kore::get(new \kore\base\KAction([
    'execute' => function () {
        return;
    }]));



