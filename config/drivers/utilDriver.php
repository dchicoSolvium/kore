<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16/04/2019
 * Time: 10:58
 */



\kore\Kore::utilDriver(new \kore\base\KObject());


\kore\Kore::utilDriver()->evaluate = function ($value, $condition, $comparation){
    switch ($condition){
        case '=':
            return $value == $comparation;

        case '==':
            return $value == $comparation;

        case '<=':
            return $value <= $comparation;

        case '>=':
            return $value >= $comparation;

        case '<':
            return $value < $comparation;

        case '>':
            return $value > $comparation;
    }    
};

\kore\Kore::utilDriver()->filter = function ($filters, $entities, \kore\base\KModel $model){
    $criteria = [];

    foreach ($filters as $field => $condition){
        // 'prop' => ['op', 'value']]
        if (is_array($condition) && isset($condition['field'])){
            $field = $condition['field'];
            $value = $condition['value'];
            $condition = $condition['condition'];
        }elseif (is_array($condition)){
            $value = $condition[1];
            $condition = $condition[0];
        }else{
            $condition = explode(' ', $condition);

            // ['prop' => 'value',
            if (count($condition) == 1){
                $value = $condition[0];
                $condition = '=';
            }else{  // 'prop' => 'op value',
                $value = $condition[1];
                $condition = $condition[0];
            }
        }

        $criteria[$field] = ['condition' => $condition, 'value' => $value];
    }

    foreach ($entities as $k => $e){
        if ($e->getModel()->getName() == $model->getName()){
            foreach ($e->getFields() as $f){
                if (isset($criteria[$f->getName()])) {
                    $c = $criteria[$f->getName()];

                    if (!\kore\Kore::utilDriver()->evaluate($e->{$f->getName()},
                        $c['condition'], $c['value'])){
                        unset($entities[$k]);
                        continue;
                    }
                }
            }
        }else{
            unset($entities[$k]);
        }
    }
    
    return array_values($entities);
};



\kore\Kore::utilDriver()->mergeEntityes = function ($entities, $newEntities){
    foreach ($entities as $k => $e){
        foreach ($newEntities as $j => $ne){
            if ($e->id == $ne->id){
                unset($newEntities[$j]);
            }
        }
    }

    $newEntities = array_values($newEntities);

    return array_merge($entities, $newEntities);
};