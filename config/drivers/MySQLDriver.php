<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16/04/2019
 * Time: 10:58
 */

include_once __DIR__ . "/utilDriver.php";

\kore\Kore::MySQLDriver(new \kore\base\KDriver());


\kore\Kore::MySQLDriver()->user = 'root';
\kore\Kore::MySQLDriver()->pass = '';
\kore\Kore::MySQLDriver()->host = '127.0.0.1';
// \kore\Kore::MySQLDriver()->port = '3306';
\kore\Kore::MySQLDriver()->database = 'korebase';

\kore\Kore::MySQLDriver()->newIds = [];


\kore\Kore::MySQLDriver()->tablePrefix = 'k_';
\kore\Kore::MySQLDriver()->propertyPrefix = 'k_';

\kore\Kore::MySQLDriver()->conn = null;


\kore\Kore::MySQLDriver()->cache = [];


\kore\Kore::MySQLDriver()->init = function (){
    $driver = \kore\Kore::kSelf();

    if ($driver->conn){
        $driver->close();
    }

    $driver->conn = new mysqli(
        $driver->host,
        $driver->user,
        $driver->pass,
        $driver->database);

    if ($driver->conn->connect_errno) {
        throw new \kore\Exceptions\KoreException('Driver error => ' . $driver->conn->connect_error);
    }
};




\kore\Kore::MySQLDriver()->close = function (){
    $driver = \kore\Kore::kSelf();

    if ($driver->conn){
        $driver->conn->close();
        $driver->conn = null;
    }
};




\kore\Kore::MySQLDriver()->setDDBB = function (){
    $driver = \kore\Kore::kSelf();
    $conn = $driver->conn;

    $models = \kore\Kore::getKore()->findByClass(\kore\base\KModel::class);

    // Create table
    foreach ($models as $m){
        $tableName = $driver->tablePrefix . $m->getName();
        $tableName = strtolower($tableName);

        $fields = $m->findByClass(\kore\base\KField::class);

        $sql = "CREATE TABLE $tableName (";

        $sep = '';
        foreach ($fields as $f){
            if (!$f->kType->database_save){
                continue;
            }

            if (!is_callable($f->kType->MySQL_Create)){
                throw new \kore\Exceptions\KoreException('Driver error: type ' .
                    $f->kType->getName() . ' needs to have "MySQL_Create" method defined to set up database, in field ' .
                    $f->getName() . ' of model ' . $m->getName()
                );
            }
            $sql .= $sep . $f->kType->MySQL_Create($driver, $f);
            $sep = ',';
        }

        $sql .= ", _delete_ BOOL not null default 0);";

        if ($conn->query($sql) !== TRUE){
            //var_dump($sql); die();
            throw new \kore\Exceptions\KoreException('Driver error => ' . $driver->conn->error);
        }

    }

};




\kore\Kore::MySQLDriver()->unsetDDBB = function (){
    $driver = \kore\Kore::kSelf();
    $conn = $driver->conn;

    $models = \kore\Kore::getKore()->findByClass(\kore\base\KModel::class);

    // Drop table
    foreach ($models as $m){
        $tableName = $driver->tablePrefix . $m->getName();
        $tableName = strtolower($tableName);

        $sql = "DROP TABLE IF EXISTS `$driver->database`.`$tableName`;";

        if ($conn->query($sql) !== TRUE){
            throw new \kore\Exceptions\KoreException('Driver error => ' . $conn->error);
        }
    }
};




\kore\Kore::MySQLDriver()->list = function (\kore\base\KModel $model, $filters = [], $orderBy=null, $limit=null, $offset=null){
    $cache = \kore\Kore::MySQLDriver()->cache;
    $cacheKey = serialize([$model->getName(), $filters]);

    if (isset($cache[$cacheKey])){
        $result = \kore\Kore::MySQLDriver()->cache[$cacheKey];
    }else{
        $result = \kore\Kore::MySQLDriver()->doList($model, $filters);
        $cache[$cacheKey] = $result;
        \kore\Kore::MySQLDriver()->cache = $cache;
    }
    
    $targetsResult = \kore\Kore::utilDriver()->filter($filters, \kore\Kore::getTargets(), $model);

    $result = \kore\Kore::utilDriver()->mergeEntityes($result, $targetsResult);

    // devolvemos un array de entidades
    \kore\Kore::checkTargets($result);
    return $result;
};


/**
 * ['prop' => 'value', // op = '=='
 * 'prop' => 'op value',
 * 'prop' => ['op', 'value']]
 * @param $filters
 */
\kore\Kore::MySQLDriver()->doList = function (\kore\base\KModel $model, $filters = [], $orderBy=null, $limit=null, $offset=null){
    $driver = \kore\Kore::kSelf();
    $conn = $driver->conn;

    if ($conn == null){
        $driver->init();
        $conn = $driver->conn;
    }

    $tableName = $driver->tablePrefix . $model->getName();
    $tableName = strtolower($tableName);

    $result = [];

    // Generamos la consulta SQL a realizar
    $sql = "SELECT * FROM $tableName WHERE ";


    $sep = " AND ";
    // Drop table
    foreach ($filters as $field => $condition){
        // 'prop' => ['op', 'value']]
        if (is_array($condition) && isset($condition['field'])){
            $field = $condition['field'];
            $value = $condition['value'];
            $condition = $condition['condition'];
        }elseif (is_array($condition)){
            $value = $condition[1];
            $condition = $condition[0];
        }else{
            $value = $condition;
            $condition = '=';
        }

        $kField = $model->$field;

        if (is_callable($kField->kType->MySQL_Insert)){
            $value = $kField->kType->MySQL_Insert($value);
        }

        $sql .= $driver->propertyPrefix . $field . ' ' . $condition . ' ' . $value . $sep;
    }


    $sql .= '_delete_ = 0';

    if ($orderBy != null) {
        $sql .= ' ORDER BY '. $driver->propertyPrefix . $orderBy . ' DESC;';
    }elseif ($model->orderBy != null) {
        $sql .= ' ORDER BY ' . $driver->propertyPrefix . $model->orderBy . ' DESC;';
    }

    if ($limit != null) {
        $sql .= ' LIMIT '. $limit;
    }

    if ($offset != null) {
        $sql .= ' OFFSET '. $offset;
    }

    $sql .= ';';


    //var_dump($sql);
    // Lanzamos la consulta
    $ret = $conn->query($sql);

    if ($ret === null || !$ret){
        var_dump($sql);
        throw new \kore\Exceptions\KoreException('Driver error => ' . $driver->conn->error);
    }

    // Parseamos los resultados y creamos el listado de entidades
    $fields = $model->findByClass(\kore\base\KField::class);

    while ($row = $ret->fetch_assoc()){
        $entity = new \kore\base\Entity($model);

        foreach ($fields as $f){
            if (!$f->kType->database_save){
                continue;
            }

            $fname = $f->getName();
            $dbname = $driver->propertyPrefix . $fname;

            if (is_callable($f->kType->MySQL_Get)){
                $entity->$fname = $f->kType->MySQL_Get($row[$dbname]);
            }else{
                $entity->$fname = $row[$dbname];
            }
        }

        $entity->modified = false;
        $entity->exists = true;
        $result[] = $entity;
    }
    
    return $result;
};



\kore\Kore::MySQLDriver()->getEntity = function (\kore\base\KModel $model, $id){
    $driver = \kore\Kore::kSelf();

    $ret = $driver->list($model, ['id' => $id]);

    \kore\Kore::checkTargets($ret);

    if (count($ret) == 0){
        return null;
    }else{
        return $ret[0];
    }
};



\kore\Kore::MySQLDriver()->getNewId = function (\kore\base\KModel $model){
    $driver = \kore\Kore::kSelf();
    $conn = $driver->conn;


    if (!isset($driver->newIds)){
        $driver->newIds = [];
        $newIds = [];
    }else{
        $newIds = $driver->newIds;
    }

    if (isset($newIds[$model->getName()])){
        $newIds[$model->getName()] = $newIds[$model->getName()] + 1;
        $driver->newIds = $newIds;
        return $driver->newIds[$model->getName()];
    }

    $tableName = $driver->tablePrefix . $model->getName();
    $tableName = strtolower($tableName);
    $idName = $driver->propertyPrefix . 'id';
    $ret = $conn->query("SELECT $idName FROM $tableName ORDER BY $idName DESC LIMIT 1");

    if ($ret === null || !$ret){
        throw new \kore\Exceptions\KoreException('Driver error => ' . $driver->conn->error);
    }

    $row = $ret->fetch_assoc();

    $newIds[$model->getName()] = $row[$idName] + 1;
    $driver->newIds = $newIds;
    return $driver->newIds[$model->getName()];
};


/**
 * @param $entitys
 * @param bool $forceParams False para que no se sobre escriban los parámetros forzosos (id)
 */
\kore\Kore::MySQLDriver()->commit = function ($entitys){
    $driver = \kore\Kore::kSelf();
    $conn = $driver->conn;
    $pref = $driver->propertyPrefix;

    if (!is_array($entitys)){
        $entitys = [ $entitys ];
    }

    foreach ($entitys as $e){
        $sql = '';
        $fields = $e->getModel()->findByClass(\kore\base\KField::class);

        if ($e->delete){
            $tableName = $driver->tablePrefix . $e->getModel()->getName();
            $tableName = strtolower($tableName);

            $fieldname = $driver->propertyPrefix . 'id';

            $sql = "UPDATE $tableName SET _delete_=1 WHERE $fieldname=$e->id";

            if ($conn->query($sql) !== TRUE){
                throw new \kore\Exceptions\KoreException('Driver error => ' . $conn->error);
            }

            continue;
        }

        if ($e->modified){
            $tableName = $driver->tablePrefix . $e->getModel()->getName();
            $tableName = strtolower($tableName);

            if ($e->exists){
                $sql .= "UPDATE $tableName SET";

                $sep = ' ';
                foreach ($fields as $f){
                    if (!$f->kType->database_save){
                        continue;
                    }

                    $fname = $f->getName();
                    $dbname = $driver->propertyPrefix . $fname;
                    $fvalue = $e->$fname;

                    if (is_callable($f->kType->MySQL_Insert)){
                        $fvalue = $f->kType->MySQL_Insert($fvalue);
                    }

                    $sql .= $sep . "$dbname=$fvalue";
                    $sep = ', ';
                }

                $sql .= " WHERE $pref" . "id = " . $e->id . ";";

            }else{
                $sql .= "INSERT INTO $tableName (";

                $sql1 = '';
                $sql2 = '';

                $sep = '';

                foreach ($fields as $f){
                    if (!$f->kType->database_save){
                        continue;
                    }

                    $fname = $f->getName();
                    $dbname = $driver->propertyPrefix . $fname;
                    $fvalue = $e->$fname;

                    if ($fvalue == null || $fvalue == $f->empty){
                        if (isset($f->default)){
                            $fvalue = $f->default;
                        }else{
                            continue;
                        }
                    }

                    if (is_callable($f->kType->MySQL_Insert)){
                        $fvalue = $f->kType->MySQL_Insert($fvalue);
                    }

                    $sql1 .= $sep . $dbname;
                    $sql2 .= $sep . $fvalue;
                    $sep = ', ';
                }

                $sql .= "$sql1) VALUES ($sql2);";
            }
            
            if ($conn->query($sql) !== TRUE){
                var_dump($sql);
                echo '<br>';
                echo $sql;
                var_dump($driver->conn->error);
                die();
                throw new \kore\Exceptions\KoreException('Driver error => ' . $driver->conn->error);
            }

            $e->modified = false;
            $e->exists = true;
        }
    }
    
    return $entitys;

};



\kore\Kore::MySQLDriver()->save = function ($file='', $save=true){
    $driver = \kore\Kore::kSelf();
    $models = \kore\Kore::getKore()->findByClass(\kore\base\KModel::class);

    $data = [];

    foreach ($models as $m){

        $entitys = [];

        foreach ($driver->list($m) as $e){
            $ent = [];
            foreach ($e->getFields() as $f){
                $fname = $f->getName();
                $ent[$fname] = $e->$fname;
            }

            $entitys[] = $ent;
        }

        $data[$m->getName()] = $entitys;
    }


    $str = json_encode($data);

    if ($save){
        file_put_contents($file, $str);
    }

    return $str;
};



\kore\Kore::MySQLDriver()->load = function ($file){
    $str = file_get_contents($file);

    $driver = \kore\Kore::kSelf();

    $array = json_decode($str, true);

    $driver->unsetDDBB();

    $driver->setDDBB();

    foreach ($array as $m => $ents){
        $kmodel = \kore\Kore::$m();
        foreach ($ents as $e){
            $entity = new \kore\base\Entity($kmodel);

            foreach ($e as $p => $v){
                $entity->$p = $v;
            }

            \kore\Kore::addTarget($entity);
        }
    }
};
