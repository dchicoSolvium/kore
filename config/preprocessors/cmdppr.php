<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */


\kore\Kore::cmdPreprocessor(new \kore\base\KPreprocess());


\kore\Kore::cmdPreprocessor()->execute = function () {
    global $argv;

    if (!isset($argv)) {
        throw new Exception();
    }

    if (count($argv) == 2) {
        \kore\Kore::$action = $argv[1];
        return;
    }

    if (count($argv) > 2) {
        \kore\Kore::$action = $argv[1];
        //\kore\Kore::$kmodel = $argv[2];
    }

};