<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */


\kore\Kore::htmlPreprocessor(new \kore\base\KPreprocess());


\kore\Kore::htmlPreprocessor()->defaultAction = '';



\kore\Kore::htmlPreprocessor()->execute = function () {
    $uri = $_SERVER['REQUEST_URI'];

    $args = [];

    // Eliminamos los argumentos Get si los hubiera
    $uri = explode ( '?' , $uri )[0];
    $uri = trim ( $uri, "/ \t\n\r\0\x0B"  );

    $data = explode('/', $uri);
    $data = array_slice ( $data , \kore\Kore::getKore()->discardFromUrl);

    if (count($data) == 3){
        \kore\Kore::$action = $data[2];
        \kore\Kore::$kmodel = $data[0];
        \kore\Kore::$params['id'] = $data[1];
    }elseif (count($data) == 2){
        \kore\Kore::$action = $data[1];
        \kore\Kore::$kmodel = $data[0];
    }elseif (count($data) == 1){
        \kore\Kore::$action = $data[0];
    }elseif (count($data) > 3){
        if (count($data) % 2 == 0){
            \kore\Kore::$action = $data[count($data) - 1];
            \kore\Kore::$kmodel = $data[count($data) - 2];
            $c = count($data) - 2;
        }else{
            \kore\Kore::$action = $data[count($data) - 1];
            \kore\Kore::$kmodel = $data[count($data) - 3];
            \kore\Kore::$params['id'] = $data[count($data) - 2];
            $c = count($data) - 3;
        }

        \kore\Kore::$params['parentPile'] = [];

        for ($i = $c - 1; $i >= 1; $i -= 2){
            \kore\Kore::$params['parentPile'][] = [
                'model' => $data[$i - 1],
                'id' => intval($data[$i])
            ];
        }


    }

    if (substr(\kore\Kore::$action, -5) == '.json'){
        \kore\Kore::$action = substr(\kore\Kore::$action, 0, -5);
        \kore\Kore::$renderer = 'apiRenderer';
    }else{
        \kore\Kore::$renderer = 'htmlRenderer';
    }

    foreach (array_keys($_GET) as $k){
        \kore\Kore::$params[$k] = urldecode($_GET[$k]);
    }

    foreach (array_keys($_POST) as $k){
        \kore\Kore::$params[$k] = urldecode($_POST[$k]);
    }

    if (!isset(\kore\Kore::$params['id'])){
        \kore\Kore::$params['id'] = -1;
    }

    return;

};
