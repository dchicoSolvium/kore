<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */

include_once __DIR__ . '/htmlrender.php';

\kore\Kore::jsonRenderer(\kore\Kore::htmlRenderer()->extend([]));
//\kore\Kore::jsonRenderer(new \kore\base\KRenderer());



\kore\Kore::jsonRenderer()->render = function (){
    $params = \kore\Kore::$params;

    ob_start();

    foreach (array_keys($params) as $k){
        if (!$params[$k]){
            unset($params[$k]);
        }
    }

    echo json_encode($params);
};

