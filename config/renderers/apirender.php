<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */


\kore\Kore::apiRenderer(new \kore\base\KRenderer());



\kore\Kore::apiRenderer()->prepareOutput = function () {
    $self = \kore\Kore::kSelf();
    $output = [];
    $data = [];

    $output['status'] = "OK";

    if (isset(\kore\Kore::$params['data'])){
        $output['data'] = \kore\Kore::$params['data'];
    }else{
        if (isset(\kore\Kore::$params['entity'])){
            $entity = \kore\Kore::$params['entity'];

            if ($entity){
                $data = $entity->jsonSerialize();
            }

        }elseif (\kore\Kore::getKModel() && \kore\Kore::$params['id'] != -1){
            \kore\Kore::$params['entity'] = \kore\Kore::getDriver()->getEntity(
                \kore\Kore::getKModel(),
                \kore\Kore::$params['id']
            );

            $data = \kore\Kore::$params['entity']->jsonSerialize();

        }elseif (isset(\kore\Kore::$list)){
            $data = [];

            foreach (\kore\Kore::$list as $l){
                $data[] = $l->jsonSerialize();
            }
        }

        $output['data'] = $data;
    }


    if (isset(\kore\Kore::$params['result'])){
        $output['result'] = \kore\Kore::$params['result'];
    }
    

    $alerts = \kore\Kore::getKore()->findByClass(\kore\base\utils\KAllert::class);

    if (count($alerts) > 0){
        $output['alerts'] = [];

        foreach ($alerts as $a){
            $output['alerts'][] = $a->jsonSerialize();
        }
    }

    return $output;
};



\kore\Kore::apiRenderer()->default = function () {
    ob_end_clean();

    $json = json_encode(\kore\Kore::apiRenderer()->prepareOutput());

    if (!$json) {
        echo json_encode([
            'status' => 'ERROR',
            'data' => ['message' => json_last_error_msg()]
        ]);
    } else {
        echo $json;
    }
};




\kore\Kore::apiRenderer()->error = function ($error) {
    $output = [];

    $output['status'] = "ERROR";

    $trace = [];

    foreach ($error->getTrace() as $t){
        $trace[] = ["function" => $t['function'],
            "file" => $t["file"],
            "line" => $t["line"]];
    }

    \kore\Kore::$params['error'] = $error->getMessage();
    \kore\Kore::$params['file'] = $error->getFile() ;
    \kore\Kore::$params['line'] = $error->getLine();
    \kore\Kore::$params['trace'] = $trace;

    if (count($alerts) > 0){
        $output['alerts'] = [];

        foreach ($alerts as $a){
            $output['alerts'][] = $a->jsonSerialize();
        }
    }

    ob_end_clean();
    echo json_encode($output);
};
