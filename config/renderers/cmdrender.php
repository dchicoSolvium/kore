<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */


\kore\Kore::cmdRender(new \kore\base\KRenderer());

\kore\Kore::$mask[] = 'cmdPreprocessor';

/**
 * Command line preprocessor
 * php "program.php" <<action>> <<model>>
 */
\kore\Kore::cmdRender()->default = function () {
    $alerts = \kore\Kore::getKore()->findByClass(\kore\base\utils\KAllert::class);

    if (count($alerts) > 0){
        echo strtoupper('KORE EXECUTED WITH ' . count($alerts) . ' ALLERTS: ') . PHP_EOL . PHP_EOL;
        foreach ($alerts as $a){
            echo strtoupper($a->type) . ': ' . $a->content . PHP_EOL;
        }
    }else{
        echo strtoupper('KORE EXECUTED SUCCESFULLY: ') . PHP_EOL . PHP_EOL;
    }

    echo 'Action: ' . \kore\Kore::$action . PHP_EOL;
    echo 'Model/Argv[1]: ' . \kore\Kore::$kmodel . PHP_EOL;
    echo 'Afected entitys: ' . count(\kore\Kore::$target) . PHP_EOL;
};

\kore\Kore::cmdRender()->clist = function () {
    echo strtoupper(\kore\Kore::$kmodel . ' LIST: ') . PHP_EOL . PHP_EOL;

    $model = \kore\Kore::getKModel();

    $fields = $model->findByClass(\kore\base\KField::class);

    $list = \kore\Kore::$list;

    $str = '';
    $str2 = '';

    foreach ($fields as $f){
        $str .= "| " . $f->getName() . "\t";
        $str2 .= "---------------";
    }

    $str .= "|" . PHP_EOL . $str2 . PHP_EOL;


    foreach ($list as $e){
        foreach ($fields as $f){
            $fname = $f->getName();
            $str .= "| " . $e->$fname . " \t";
        }
        $str .= "|" . PHP_EOL;
    }

    echo $str . $str2 . PHP_EOL;
};


\kore\Kore::cmdRender()->error = function ($error) {
    $code = $error->getCode();
    $msg = $error->getMessage();

    echo "KORE ERROR ($code): $msg" . PHP_EOL;

    die();
};