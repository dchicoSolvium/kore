<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */


\kore\Kore::htmlRenderer(new \kore\base\KRenderer());



\kore\Kore::htmlRenderer()->prepareParams = function () {
    $self = \kore\Kore::kSelf();

    if (\kore\Kore::getKModel()){
        \kore\Kore::$params['model'] = \kore\Kore::getKModel();
        \kore\Kore::$params['fields'] = \kore\Kore::getKModel()->getFields();

        if (!isset(\kore\Kore::$params['entity'])){
            if (\kore\Kore::$params['id'] == -1){
                \kore\Kore::$params['entity'] = new \kore\base\Entity(\kore\Kore::getKModel());
            }else{
                \kore\Kore::$params['entity'] = \kore\Kore::getDriver()->getEntity(
                    \kore\Kore::getKModel(),
                    \kore\Kore::$params['id']
                );
            }
        }

        if (\kore\Kore::getKModel()->title){
            \kore\Kore::$params['title'] = \kore\Kore::getKModel()->title;
        }else{
            \kore\Kore::$params['title'] = \kore\Kore::$kmodel;
        }
    }

    \kore\Kore::$params['baseURL'] = \kore\Kore::getKore()->baseURL;
    
    \kore\Kore::$params['list'] = \kore\Kore::$list;
    \kore\Kore::$params['targets'] = \kore\Kore::$target;
    \kore\Kore::$params['action'] = \kore\Kore::$action;

    if (isset(\kore\Kore::$params['parentPile'])){
        $ppStack = [];
        foreach (\kore\Kore::$params['parentPile'] as $i => $pp){
            $auth = \kore\Kore::auth();
            if ($auth != null && $auth->authModel != null){
                if ($pp['model'] == $auth->authModel){
                    unset(\kore\Kore::$params['parentPile'][$i]);
                    continue;
                }
            }

            $model = \kore\Kore::getKore()->{$pp['model']};

            $ent = \kore\Kore::getDriver()->getEntity(
                $model,
                $pp['id']
            );

            if ($ent == null){
                unset(\kore\Kore::$params['parentPile'][$i]);
            }else{
                \kore\Kore::$params['parentPile'][$i]['name'] = $ent->toString();
                \kore\Kore::$params['parentPile'][$i]['url'] = $self->createURL('table', -1, $pp['model'], false, $ppStack);
                $ppStack[$pp['model']] = $pp['id'];
            }
        }

        \kore\Kore::$params['parentPile'] = array_values(\kore\Kore::$params['parentPile']);
    }

    \kore\Kore::$params['user'] = \kore\Kore::auth()->getUser();
    //\kore\Kore::$params['menu'] = 'menu';


    if (!isset(\kore\Kore::$params['id'])){
        \kore\Kore::$params['id'] = -1;
    }



    \kore\Kore::$params['alerts'] = \kore\Kore::getKore()->findByClass(\kore\base\utils\KAllert::class);
};



\kore\Kore::htmlRenderer()->dirs = [
    __DIR__ . "/../../resources/views"
];

\kore\Kore::htmlRenderer()->addDir = function ($dir) {
    $renderer = \kore\Kore::kSelf();

    $dirs = \kore\Kore::htmlRenderer()->dirs;
    $dirs[] = $dir;

    $renderer->dirs = $dirs;
};

\kore\Kore::htmlRenderer()->cache = __DIR__ . "/../../resources/cache";


\kore\Kore::htmlRenderer()->preview = function ($template, $params = []){
    $renderer = \kore\Kore::kSelf();
    $renderer->prepareParams();

    $params = array_merge($params, \kore\Kore::$params);
    $dirs = $renderer->dirs;

    for ($i = count($dirs) - 1; $i >= 0; $i--){
        try{
            $blade = new \Jenssegers\Blade\Blade($dirs[$i], $renderer->cache);

            return $blade->render($template, $params);
        }catch (Exception $e){
            continue;
        }
    }

    throw new Exception("Template $template could not be found");
};


\kore\Kore::htmlRenderer()->render = function ($template, $params = []){
    $renderer = \kore\Kore::kSelf();
    $renderer->prepareParams();

    $params = array_merge($params, \kore\Kore::$params);

    ob_start();

    $dirs = $renderer->dirs;

    for ($i = count($dirs) - 1; $i >= 0; $i--){
        try{
            $blade = new \Jenssegers\Blade\Blade($dirs[$i], $renderer->cache);

            echo $blade->make($template, $params);
            return;

        }catch (Exception $e){
            continue;
        }
    }

    throw new Exception("Template $template could not be found");
};


\kore\Kore::htmlRenderer()->createURL = function ($action="table", $id=-1, $model=null, $useParentPile=true, $parents = []){
    if (!$model){
        $model = \kore\Kore::$kmodel;    
    }

    $pile = '';
    if ($useParentPile && isset(\kore\Kore::$params['parentPile'])){
        foreach (\kore\Kore::$params['parentPile'] as $p){
            $pile .= "/" . $p["model"] . "/" . $p["id"];
        }
    }

    if (count($parents) > 0){
        foreach ($parents as $p => $id){
            $pile .= "/" . $p . "/" . $id;
        }
    }

    if (isset(\kore\Kore::$params['nomenu'])){
        return \kore\Kore::getKore()->baseURL . $pile . "/$model/$id/$action?nomenu=true";
    }

    return \kore\Kore::getKore()->baseURL . $pile . "/$model/$id/$action";
};


\kore\Kore::htmlRenderer()->default = function () {

};



\kore\Kore::htmlRenderer()->error = function ($error) {
    $self = \kore\Kore::kSelf();

    ob_end_clean();

    $self->prepareParams();

    $str = '';

    $str .= '<strong>KORE ERROR: </strong>' .
        $error->getMessage() . ' in ' .
        $error->getFile() . ' (' . $error->getLine() . '):';

    $str .= '<ul>';
    foreach ($error->getTrace() as $t){
        $str .= '<li>Function ' . $t['function'];
        if (isset($t["file"])){
            $str .= ' in ' . $t["file"] . ' (' . $t["line"] . ')</li>';
        }
    }
    $str .= '</ul>';

    \kore\Kore::$params['error'] = new \kore\base\utils\KAllert(
        $str, 'danger'
    );

    try{
        $self->table();
    }catch (Exception $e){
        $self->render('default.default');
    }
};


\kore\Kore::htmlRenderer()->table = function () {
    $self = \kore\Kore::kSelf();
    $model = \kore\Kore::getKModel();
    $list = \kore\Kore::$list;

    $self->prepareParams();
    
    $actions = $model->actions;

    $data = [];

    $tableFields = $model->getTableFields();

    if (is_array($list)){
        foreach ($list as $e){
            $row = [];
            foreach ($tableFields as $t){
                $t->getName();
                $row[] = $t->kType->toTableString($e->{$t->getName()}, $t);
            }

            if ($actions) {
                $str = '';
                $sep = '';

                foreach ($actions as $a) {
                    if (is_string($a)){
                        $dir = $self->createURL($a, $e->id);
                        $str .= $sep . '<a href=' . $dir . '>' . $a . '</a>';
                        $sep = ' | ';
                    }elseif (is_array($a)){
                        $parents = [];

                        if (isset($a['parents'])){
                            foreach ($a['parents'] as $p){
                                $parentField = \kore\Kore::kFK()->parentField($p, $model);

                                if ($parentField){
                                    $parents[$p] = $e->{$parentField->getName()};
                                }
                            }
                        }

                        //$parents[$model->getName()] = $e->id;

                        $dir = $self->createURL($a['action'], $e->id, null, true, $parents);
                        $str .= $sep . '<a href=' . $dir . '>' . $a['desc'] . '</a>';
                        $sep = ' | ';
                    }
                }

                $row[] = $str;
            }

            $data[] = $row;
        }
    }

    $headFields = [];

    foreach ($tableFields as $t){
        $headFields[] = ($t->label) ? $t->label : $t->getName();
    }

    if ($actions){
        $headFields[] = '';
    }


    \kore\Kore::$params['tableFields'] = $headFields;
    \kore\Kore::$params['data_json'] = json_encode($data);
    \kore\Kore::$params['data'] = $data;

    \kore\Kore::$params['create'] = \kore\Kore::htmlRenderer()->createURL('create');
    \kore\Kore::$params['url'] = \kore\Kore::htmlRenderer()->createURL();

    if ($model->tableTemplate != null){
        $self->render($model->tableTemplate);
    }else{
        $self->render('base.table');
    }

};


\kore\Kore::htmlRenderer()->create = function () {
    $self = \kore\Kore::kSelf();
    $self->prepareParams();

    \kore\Kore::$params['fields'] = \kore\Kore::getKModel()->getCreateFields();

    if (isset($model->formTemplate)){
        $self->render($model->formTemplate);
    }else{
        $self->render('base.form');
    }
};

\kore\Kore::htmlRenderer()->edit = function () {
    $self = \kore\Kore::kSelf();
    $self->prepareParams();

    $model = \kore\Kore::getKModel();

    \kore\Kore::$params['fields'] = $model->getEditFields();

    if (isset($model->formTemplate)){
        $self->render($model->formTemplate);
    }else{
        $self->render('base.form');    
    }
};

\kore\Kore::htmlRenderer()->delete = function () {
    $self = \kore\Kore::kSelf();
    $self->prepareParams();

    \kore\KEngine::reAction('table');
};

\kore\Kore::htmlRenderer()->parse = function () {
    $self = \kore\Kore::kSelf();
    $self->prepareParams();
    
    if (\kore\Kore::getKore()->hascommit){
        \kore\KEngine::reAction('table');
    }else{
        if (\kore\Kore::$params['id'] == -1){
            \kore\KEngine::reAction('create');
        }else{
            \kore\KEngine::reAction('edit');
        }
    }
};
