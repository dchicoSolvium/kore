<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 24/05/2019
 * Time: 10:12
 */


use kore\Kore;

Kore::parentPileServc(new \kore\base\KService());


Kore::parentPileServc()->getId = function ($model){
    if (!isset(\kore\Kore::$params['parentPile'])){
        return -1;
    }

    foreach (\kore\Kore::$params['parentPile'] as $p){
        if ($p['model'] == $model){
            return $p['id'];
        }
    }
    
    return -1;
};


Kore::parentPileServc()->removeDuplicates = function ($parentPile){
    $newParentPile = [];

    $models = [];

    foreach ($parentPile as $pp){
        if (!in_array($pp['model'], $models)){
            $newParentPile[] = $pp;
            $models[] = $pp['model'];
        }
    }

    return $newParentPile;
};

Kore::parentPileServc()->orderPile = function ($parentPile){
    usort($parentPile, function ($pp1, $pp2){
        $model1 = Kore::getKore()->{$pp1['model']};
        $model2 = Kore::getKore()->{$pp2['model']};

        $f1 = \kore\Kore::kFK()->parentField($model1, $model2) == null;
        $f2 = \kore\Kore::kFK()->parentField($model2, $model1) == null;

        if (!$f1 && !$f2 || $f1 && $f2){
            return 0;
        }

        if ($f1){
            return 1;
        }

        if ($f2){
            return -1;
        }
    });

    return $parentPile;
};


Kore::parentPileServc()->add = function ($model, $id){
    $flag = false;
    
    if (!isset(\kore\Kore::$params['parentPile'])){
        \kore\Kore::$params['parentPile'] = [];
    }
    
    foreach (\kore\Kore::$params['parentPile'] as $k => $p){
        if ($p['model'] == $model){
            \kore\Kore::$params['parentPile'][$k]['id'] = $id;
            $flag = true;
        }
    }

    if (!$flag){
        \kore\Kore::$params['parentPile'][] = [
            'model' => $model,
            'id' => $id
        ];
    }
};


Kore::parentPileServc()->execute = function(){
    
    $model = Kore::getKModel();

    if (!isset(\kore\Kore::$params['parentPile'])){
        return;
    }

    \kore\Kore::$params['parentPile'] = Kore::parentPileServc()->removeDuplicates(\kore\Kore::$params['parentPile']);
    \kore\Kore::$params['parentPile'] = Kore::parentPileServc()->orderPile(\kore\Kore::$params['parentPile']);

    foreach (\kore\Kore::$params['parentPile'] as $p){
        // Por defecto, todos los targets, si tienen un campo kFK con el modelo padre,
        // se le asigna el valor de filtrado
        foreach (Kore::getTargets() as $t){
            $f = \kore\Kore::kFK()->parentField($p['model'], $t->getModel());
            if ($f){
                $t->{$f->getName()} = $p['id'];
            }    
        }


        // Eliminamos el elemento del parent pile de las listas de output, de tal forma que es invisible
        $f = \kore\Kore::kFK()->parentField($p['model'], $model);

        if (!$f){
            continue;
        }

        $tableFields = $model->getTableFields(true);
        $editFields = $model->getEditFields(true);
        $createFields = $model->getCreateFields(true);

        if ($i = array_search($f->getName(), $tableFields)){
            unset($tableFields[$i]);
            $model->tableFields = array_values($tableFields);
        }

        if ($i = array_search($f->getName(), $editFields)){
            unset($editFields[$i]);
            $model->editFields = array_values($editFields);
        }

        if ($i = array_search($f->getName(), $createFields)){
            unset($createFields[$i]);
            $model->createFields = array_values($createFields);
        }

    }
    
};