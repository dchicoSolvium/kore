<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16/04/2019
 * Time: 10:58
 */

include_once __DIR__ . '/../types/basetypes.php';

\kore\Kore::metakomp(new \kore\base\Komponent());


\kore\Kore::metakomp()->metaFields = [
    new \kore\base\KField([
        'invisible' => true
    ], 'kDatetime', '_lastChange_'),
    new \kore\base\KField([
        'invisible' => true,
        'default' => false
    ], 'kDatetime', '_created_')
];


\kore\Kore::metakomp()->preprocess = function () {
    $models = \kore\Kore::getKore()->findByClass(\kore\base\KModel::class);
    
    foreach ($models as $m) {
        foreach (\kore\Kore::metakomp()->metaFields as $f) {
            $m->{$f->getName()} = $f;
        }
    }

};


\kore\Kore::metakomp()->postprocess = function () {
    foreach (\kore\Kore::$target as $t) {
        if (!$t->exists){
            $t->_created_ = new DateTime();
            $t->_lastChange_ = new DateTime();
        }elseif ($t->modified){
            $t->_lastChange_ = new DateTime();
        }

    }

};


