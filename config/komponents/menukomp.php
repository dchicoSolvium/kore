<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16/04/2019
 * Time: 10:58
 */

include_once __DIR__ . '/../types/basetypes.php';

use kore\Kore;

Kore::menukomp(new \kore\base\Komponent());




Kore::menukomp()->postprocess = function () {
    if (Kore::$renderer != 'htmlRenderer'){
        return;
    }

    if (isset(Kore::$params['nomenu'])){
        Kore::$params['menu'] = null;
        return;
    }

    $user = Kore::auth()->getUser();

    if (!$user){
        return;
    }

    $c = count(Kore::$params['menu']);

    for ($i = 0; $i < $c; $i++){
        $m = Kore::$params['menu'][$i];

        if (!isset(Kore::$params['menu'][$i]['link'])){
            Kore::$params['menu'][$i]['link'] = '#';
        }

        if (isset($m['submenu'])){
            // Llamamos recursivamente a esta función
            $_m = Kore::$params['menu'];    // Aseguramos el menú actual y lo sobrescribimos con el submenu
            Kore::$params['menu'] = $m['submenu'];

            Kore::menukomp()->postprocess();

            $__m = Kore::$params['menu'];
            Kore::$params['menu'] = $_m;
            Kore::$params['menu'][$i]['submenu'] = $__m;

            Kore::$params['menu'][$i]['link'] = '#';

            if (count($__m) == 0){
                unset(Kore::$params['menu'][$i]);
            }

        }elseif (isset($m['model'])){
            if (Kore::authUser()->userCan($user, $m['model'])){
                Kore::$params['menu'][$i]['link'] = Kore::htmlRenderer()->createURL('table', -1, $m['model'], false);
            }else{
                unset(Kore::$params['menu'][$i]);
            }
        }elseif (isset($m['freeAction'])){
            if (Kore::authUser()->userCan($user, null, $m['freeAction'])){
                Kore::$params['menu'][$i]['link'] = Kore::htmlRenderer()->createURL($m['freeAction'], -1, null, false);
            }else{
                unset(Kore::$params['menu'][$i]);
            }
        }
    }

    Kore::$params['menu'] = array_values(Kore::$params['menu']);
    
};


