<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 9:06
 */

include "vendor/autoload.php";

use kore\Kore;
use kore\KEngine;

Kore::$preprocessor = 'cmdPreprocessor';
Kore::$renderer = 'cmdRender';

KEngine::play();

