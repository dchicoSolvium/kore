<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 9:06
 */

include "../vendor/autoload.php";



\kore\Kore::addConfig(__DIR__ . '/../chronos/model');
\kore\Kore::addConfig(__DIR__ . '/../chronos/rules');
\kore\Kore::addConfig(__DIR__ . '/../bin');

\kore\KEngine::play();

