<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 9:06
 */

include "../vendor/autoload.php";

use kore\base\KObject;

use kore\Kore;


Kore::kSelf();

Kore::kfloat(new KObject());

Kore::kfloat()->value = 5.0;

Kore::kfloat()->print = function (){
  echo 'Valor: ' . Kore::kSelf()->value . PHP_EOL;
};


Kore::Preprocess(new KObject());
Kore::Preprocess_SimplePreprocess(new KObject([
    'prop' => 2
]));
Kore::Preprocess_SimplePreprocess_HTMLPreprocess(new KObject([], "HTMLPreprocess"));

Kore::Preprocess()->SimplePreprocess->NewPreprocess = new KObject();

Kore::Preprocess()->SimplePreprocess->NewPreprocess->anotherOne = new KObject();

Kore::Preprocess(new KObject([
    'execute' => function () {}], "NewPreprocess"));

Kore::HTMLPreprocess()->execute = function () {};

//Kore::getKore()->dump();

Kore::kfloat()->print();

//var_dump(Kore::Preprocess_HTMLPreprocess());

Kore::getKore()->dump();


// Pruebas a realizar:

//- Extends
//- Property array
//- Find
//- Carga de ficheros de configuración correcta
//- ForceFit
//- Validación de fitModel

// TODO

//- Sistema de warnings y errores de configuración
//- Ejecución de komponents
//- Driver básico
//- Objeto entidad y operaciones con el objeto entidad

